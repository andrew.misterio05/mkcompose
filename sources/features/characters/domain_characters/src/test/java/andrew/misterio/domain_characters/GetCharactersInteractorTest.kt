package andrew.misterio.domain_characters

import kotlinx.coroutines.test.runTest
import org.junit.Test
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

class GetCharactersInteractorTest {
    private val repo = mock<CharactersRemoteRepo>()
    private val subject = GetCharactersInteractor(repo)
    private val character: Character = mock()

    @Test
    fun checkCount() = runTest {
        (0..100).forEach {
            val times = it
            val list = mutableListOf<Character>().apply {
                repeat(times) { add(character) }
            }
            whenever(repo.getCharacters()).doReturn(list)
            assert(subject.execute().count() == times)
        }
    }

    @Test
    fun checkContent() = runTest {
        val character = Character("1", "2", "3")
        val list = listOf(character)
        whenever(repo.getCharacters()).doReturn(list)
        assert(subject.execute().first() == character)
    }
}
