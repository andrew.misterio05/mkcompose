package andrew.misterio.domain_characters

fun interface CharactersRemoteRepo {
    suspend fun getCharacters(): List<Character>
}
