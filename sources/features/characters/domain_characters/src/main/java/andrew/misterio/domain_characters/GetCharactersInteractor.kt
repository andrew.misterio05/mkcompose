package andrew.misterio.domain_characters

class GetCharactersInteractor(
    private val repository: CharactersRemoteRepo
) {
    suspend fun execute(): List<Character> = repository.getCharacters()
}
