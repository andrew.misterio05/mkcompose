package andrew.misterio.domain_characters

data class Character(
    val id: String,
    val image: String,
    val name: String,
)
