package andrew.misterio.ui_characters

import andrew.misterio.core.koin.di
import andrew.misterio.domain_characters.Character
import andrew.misterio.domain_characters.CharactersRemoteRepo
import andrew.misterio.domain_characters.GetCharactersInteractor
import andrew.misterio.ui_characters.presentation.CharactersViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.parameter.parametersOf

internal const val SCOPE = "characters scope"

private typealias GetList = suspend () -> List<Character>

val diCharacters = di(
    name = SCOPE,
) {
    scoped { (getList: GetList) ->
        GetCharactersInteractor(
            repository = get { parametersOf(getList) }
        )
    }
    viewModel { (getList: GetList, columns: Int) ->
        CharactersViewModel(get { parametersOf(getList) }, columns)
    }
    scoped { (getList: GetList) -> CharactersRemoteRepo(getList) }
}
