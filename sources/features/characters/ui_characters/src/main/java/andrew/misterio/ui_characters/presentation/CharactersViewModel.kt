package andrew.misterio.ui_characters.presentation

import andrew.misterio.core.BaseViewModel
import andrew.misterio.domain_characters.Character
import andrew.misterio.domain_characters.GetCharactersInteractor
import org.orbitmvi.orbit.syntax.simple.postSideEffect
import org.orbitmvi.orbit.syntax.simple.reduce

sealed class Event {
    sealed class Navigation : Event() {
        data class ToDetails(val id: String) : Navigation()
    }

    object OnFirstDataReady : Event()
}

sealed class State {
    data class Data(
        val list: List<CharacterViewData>,
        val columns: Int,
    ) : State()
    object Progress : State()
}

class CharactersViewModel(
    private val getCharactersInteractor: GetCharactersInteractor,
    private val columns: Int,
) : BaseViewModel<State, Event>(State.Progress) {

    init {
        intent {
            val data = getCharactersInteractor.execute().map(::mapItem)
            reduce { State.Data(data, columns) }
            postSideEffect(Event.OnFirstDataReady)
        }
    }

    fun onCharacterClick(id: String) = intent {
        postSideEffect(Event.Navigation.ToDetails(id))
    }

    private fun mapItem(it: Character) = CharacterViewData(it.id, it.image, it.name.uppercase())
}
