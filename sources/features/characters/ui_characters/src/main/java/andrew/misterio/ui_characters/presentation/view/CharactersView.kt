@file:OptIn(ExperimentalComposeUiApi::class)

package andrew.misterio.ui_characters.presentation.view

import andrew.misterio.core.screenHeight
import andrew.misterio.core.screenWidth
import andrew.misterio.core.shared_elements.LocalSharedContainer
import andrew.misterio.design.dimens
import andrew.misterio.design.list.DelegatedLazyVerticalGrid
import andrew.misterio.design.list.ListViewItem
import andrew.misterio.design.list.verticalGridDelegate
import andrew.misterio.ui_characters.presentation.CharacterViewData
import andrew.misterio.ui_characters.presentation.SharedItemPool
import andrew.misterio.ui_characters.presentation.State
import android.net.Uri
import android.view.MotionEvent
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.input.pointer.pointerInteropFilter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import arrow.core.partially1
import coil.compose.AsyncImage
import kotlin.math.max

@Composable
internal fun CharactersView(
    columns: Int,
    state: State,
    onClick: (String) -> Unit
) = Box(modifier = Modifier.background(MaterialTheme.colors.background)) {
    if (columns <= 0) return

    val shape = MaterialTheme.shapes.run { if (columns > 1) medium else large }
    val smallPaddings = MaterialTheme.dimens.paddings.small
    val listWidth = screenWidth - smallPaddings * 2
    val columnDividerCount = max(columns - 1, 1)
    val itemHeight = listWidth / columnDividerCount
    val itemWidth = listWidth / columns

    val itemsOnScreen = ((screenHeight / itemHeight).toInt() + 2) * columns

    val sharedContainer = remember { SharedItemPool(itemsOnScreen) }

    (state as? State.Data)?.list?.let { list ->
        DelegatedLazyVerticalGrid<ListViewItem>(
            items = list,
            modifier = Modifier
                .fillMaxHeight()
                .padding(smallPaddings),
            rowSize = columns,
            verticalArrangement = Arrangement.Center,
            horizontalArrangement = Arrangement.Center,
            delegates = arrayOf(
                verticalGridDelegate<CharacterViewData> {
                    val selected = remember { mutableStateOf(false) }
                    val scale = animateFloatAsState(if (selected.value) 0.95f else 1f)
                    DisposableEffect(key1 = it.id) {
                        onDispose {
                            sharedContainer.onDispose(it.id)
                        }
                    }
                    val sharedItemContainer = LocalSharedContainer.current
                    Character(
                        data = it,
                        modifier = Modifier
                            .size(height = itemHeight, width = itemWidth)
                            .scale(scale.value)
                            .padding(smallPaddings)
                            .pointerInteropFilter { event ->
                                when (event.action) {
                                    MotionEvent.ACTION_DOWN -> selected.value = true
                                    MotionEvent.ACTION_UP -> {
                                        sharedItemContainer[it.id] = sharedContainer.createSharedItem(it.id)
                                        selected.value = false
                                        onClick(it.id)
                                    }
                                    MotionEvent.ACTION_CANCEL -> selected.value = false
                                }
                                true
                            }
                            .shadow(elevation = MaterialTheme.dimens.elevation.small, shape = shape)
                            .background(
                                color = MaterialTheme.colors.surface,
                                shape = shape
                            )
                            .border(
                                width = 1.dp,
                                color = MaterialTheme.colors.primary,
                                shape = shape
                            )
                            .onGloballyPositioned(sharedContainer::saveItem.partially1(it.id))
                    )
                }
            )
        )
    }
}

@Composable
private fun Character(
    data: CharacterViewData,
    modifier: Modifier
) = Box(
    modifier = modifier,
    contentAlignment = Alignment.Center
) {
    AsyncImage(
        model = Uri.parse("file:///android_asset/" + data.image),
        modifier = Modifier.fillMaxSize(),
        contentDescription = "",
        contentScale = ContentScale.Crop
    )
    Text(
        modifier = Modifier
            .fillMaxWidth()
            .background(color = MaterialTheme.colors.background.copy(alpha = 0.7f))
            .align(Alignment.BottomCenter)
            .padding(vertical = MaterialTheme.dimens.paddings.small),
        text = data.name,
        textAlign = TextAlign.Center,
        style = TextStyle(color = MaterialTheme.colors.primary)
    )
}
