package andrew.misterio.ui_characters.presentation.view

import andrew.misterio.core.CollectEvents
import andrew.misterio.core.koin.KoinScope
import andrew.misterio.core.koin.getViewModel
import andrew.misterio.core.state
import andrew.misterio.domain_characters.Character
import andrew.misterio.ui_characters.SCOPE
import andrew.misterio.ui_characters.presentation.CharactersViewModel
import andrew.misterio.ui_characters.presentation.Event
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import org.koin.core.parameter.parametersOf

@Composable
fun CharactersScreen(
    columns: Int,
    getList: suspend () -> List<Character>,
    navigateTo: (String) -> Unit,
    onDataReady: () -> Unit = {},
) = KoinScope(SCOPE) {
    val vm: CharactersViewModel = getViewModel(
        parameters = { parametersOf(getList, columns) }
    )
    val state by vm.state()
    vm.CollectEvents {
        when (it) {
            is Event.Navigation.ToDetails -> navigateTo(it.id)
            is Event.OnFirstDataReady -> onDataReady()
        }
    }
    CharactersView(
        state = state,
        onClick = vm::onCharacterClick,
        columns = columns,
    )
}
