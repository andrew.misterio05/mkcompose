package andrew.misterio.ui_characters.presentation

import andrew.misterio.core.shared_elements.SharedItem
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.layout.LayoutCoordinates
import androidx.compose.ui.layout.positionInWindow
import androidx.compose.ui.unit.IntSize
import androidx.core.util.Pools
import arrow.core.partially1

class SharedItemPool(size: Int) {
    private val pool: Pools.Pool<Params> = Pools.SimplePool(size)
    private val container = mutableMapOf<String, Params>()

    init {
        repeat(size) {
            pool.release(Params(Offset.Zero, IntSize.Zero))
        }
    }

    fun saveItem(id: String, layoutCoordinates: LayoutCoordinates) {
        pool.acquire()
            ?.also {
                it.position = layoutCoordinates.positionInWindow()
                it.size = layoutCoordinates.size
            }
            ?.let(container::set.partially1(id))
    }

    fun onDispose(id: String) {
        container.remove(id)?.let(pool::release)
    }

    fun createSharedItem(id: String): SharedItem = container[id]?.let {
        SharedItem.Params(
            position = it.position,
            size = it.size,
        )
    }
        ?: SharedItem.Unknown

    data class Params(
        var position: Offset,
        var size: IntSize,
    )
}
