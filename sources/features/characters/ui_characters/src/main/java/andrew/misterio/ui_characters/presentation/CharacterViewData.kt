package andrew.misterio.ui_characters.presentation

import andrew.misterio.design.list.ListViewItem

data class CharacterViewData(
    val id: String,
    val image: String,
    val name: String,
) : ListViewItem {
    override val key: ListViewItem.Key = CharacterViewData

    companion object : ListViewItem.Key
}
