package andrew.misterio.domain_home

import kotlinx.coroutines.test.runTest
import org.junit.Test

class GetHomeItemsInteractorTest {

    @Test
    fun `at least one item`() = runTest {
        val subject = GetHomeItemsInteractor()
        assert(subject.execute().count() > 0)
    }
}
