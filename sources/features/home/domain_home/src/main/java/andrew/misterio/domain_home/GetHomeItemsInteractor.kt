package andrew.misterio.domain_home

import kotlinx.coroutines.delay

class GetHomeItemsInteractor {
    suspend fun execute(): List<HomeItem> {
        delay(100)
        return listOf(
            HomeItem(
                id = HomeItem.Id.MK11,
                imageUrl = "https://assets.www.warnerbros.com/drupal-root/public/mortal_kombat_11_aftermath_key_art_800.jpg"
            ),
            /*HomeItem(
                id = HomeItem.Id.DOTA2,
                imageUrl = "https://images2.alphacoders.com/474/thumb-1920-474206.jpg"
            )*/
        )
    }
}
