package andrew.misterio.domain_home

data class HomeItem(
    val id: Id,
    val imageUrl: String
) {
    enum class Id {
        DOTA2, MK11
    }
}
