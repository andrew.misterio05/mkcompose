package andrew.misterio.ui_home

import andrew.misterio.common.arrow.Lambda
import andrew.misterio.core.CollectEvents
import andrew.misterio.core.koin.KoinScope
import andrew.misterio.core.koin.getViewModel
import andrew.misterio.core.state
import andrew.misterio.ui_home.view.HomeList
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier

@Composable
fun HomeScreen(
    router: HomeRouter,
    onDataReady: Lambda,
) = KoinScope(SCOPE) {
    val vm: HomeViewModel = getViewModel()
    val state by vm.state()
    vm.CollectEvents {
        when (it) {
            is HomeEvent.Navigation -> {
                when (it) {
                    is HomeEvent.Navigation.ToMk11 -> router.toMk11
                    is HomeEvent.Navigation.ToDota2 -> router.toDota2
                }
                    .invoke(it.isSingleTop)
            }
            is HomeEvent.OnFirstDataReady -> onDataReady()
        }
    }
    Box(modifier = Modifier.background(MaterialTheme.colors.background)) {
        when (state) {
            is HomeState.Data -> {
                HomeList(
                    list = (state as HomeState.Data).list,
                    onGameClick = vm::onGameClick
                )
            }
            HomeState.Loading -> Box(modifier = Modifier.fillMaxSize()) {
                CircularProgressIndicator(modifier = Modifier.align(Alignment.Center))
            }
        }
    }
}
