package andrew.misterio.ui_home

import andrew.misterio.core.BaseViewModel
import andrew.misterio.design.list.ListViewItem
import andrew.misterio.domain_home.GetHomeItemsInteractor
import andrew.misterio.domain_home.HomeItem
import org.orbitmvi.orbit.syntax.simple.postSideEffect
import org.orbitmvi.orbit.syntax.simple.reduce

sealed class HomeEvent {
    sealed class Navigation : HomeEvent() {
        abstract val isSingleTop: Boolean

        data class ToMk11(override val isSingleTop: Boolean) : Navigation()
        data class ToDota2(override val isSingleTop: Boolean) : Navigation()
    }

    object OnFirstDataReady : HomeEvent()
}

sealed class HomeState {
    data class Data(val list: List<ListViewItem>) : HomeState() {
        data class Item(val id: HomeItem.Id, val imageUrl: String) : ListViewItem {
            override val key: ListViewItem.Key get() = Item

            companion object : ListViewItem.Key
        }
    }

    object Loading : HomeState()
}

class HomeViewModel(
    private val getHomeItemsInteractor: GetHomeItemsInteractor,
    private val mapper: HomeMapper,
) : BaseViewModel<HomeState, HomeEvent>(HomeState.Loading) {

    init {
        load()
    }

    fun onGameClick(id: HomeItem.Id) = intent {
        postSideEffect(mapper.toEvent(id, isSingle = false))
    }

    private fun load() = intent {
        val list = getHomeItemsInteractor.execute()
        when {
            list.count() == 1 -> {
                postSideEffect(
                    mapper.toEvent(
                        id = list.first().id,
                        isSingle = true
                    )
                )
            }
            else -> {
                reduce { HomeState.Data(list = mapper.toUiItems(list)) }
                postSideEffect(HomeEvent.OnFirstDataReady)
            }
        }
    }
}
