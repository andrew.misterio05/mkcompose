package andrew.misterio.ui_home

import andrew.misterio.core.koin.di
import andrew.misterio.domain_home.GetHomeItemsInteractor
import org.koin.androidx.viewmodel.dsl.viewModel

internal const val SCOPE = "home scope"

val diHome = di(SCOPE) {
    factory { HomeMapper() }
    factory { GetHomeItemsInteractor() }
    viewModel { HomeViewModel(get(), get()) }
}
