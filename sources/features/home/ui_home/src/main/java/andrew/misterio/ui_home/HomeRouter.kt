package andrew.misterio.ui_home

import andrew.misterio.common.arrow.Lambda1

data class HomeRouter(
    val toMk11: Lambda1<Boolean>,
    val toDota2: Lambda1<Boolean>,
)
