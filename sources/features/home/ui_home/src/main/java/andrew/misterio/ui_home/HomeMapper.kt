package andrew.misterio.ui_home

import andrew.misterio.design.list.ListViewItem
import andrew.misterio.design.list.SeparatorViewItem
import andrew.misterio.domain_home.HomeItem
import androidx.compose.ui.unit.dp

class HomeMapper {

    fun toEvent(id: HomeItem.Id, isSingle: Boolean): HomeEvent = when (id) {
        HomeItem.Id.MK11 -> HomeEvent.Navigation.ToMk11(isSingle)
        HomeItem.Id.DOTA2 -> HomeEvent.Navigation.ToDota2(isSingle)
    }

    fun toUiItems(list: List<HomeItem>): List<ListViewItem> {
        val separator = SeparatorViewItem(thickness = 8.dp)
        return list.flatMap { homeItem ->
            listOf(
                separator,
                HomeState.Data.Item(
                    id = homeItem.id,
                    imageUrl = homeItem.imageUrl
                )
            )
        } + separator
    }
}
