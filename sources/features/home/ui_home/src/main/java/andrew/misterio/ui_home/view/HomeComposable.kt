package andrew.misterio.ui_home.view

import andrew.misterio.design.dimens
import andrew.misterio.design.list.DelegatedLazyList
import andrew.misterio.design.list.ListViewItem
import andrew.misterio.design.list.Orientation
import andrew.misterio.design.list.listDelegate
import andrew.misterio.design.list.separatorVerticalDelegate
import andrew.misterio.domain_home.HomeItem
import andrew.misterio.ui_home.HomeState
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.layout.ContentScale
import arrow.core.andThen
import coil.compose.AsyncImage

@Composable
internal fun HomeList(
    list: List<ListViewItem>,
    onGameClick: (HomeItem.Id) -> Unit
) = DelegatedLazyList(
    items = list,
    modifier = Modifier.fillMaxHeight(),
    orientation = Orientation.Vertical(
        alignment = Alignment.CenterHorizontally,
        arrangement = Arrangement.Center
    ),
    delegates = mapOf(
        listDelegate<HomeState.Data.Item>(HomeState.Data.Item) {
            ItemComposable(
                onItemClick = HomeState.Data.Item::id andThen onGameClick,
                data = it
            )
        },
        separatorVerticalDelegate()
    )
)

@Composable
private fun ItemComposable(
    onItemClick: (HomeState.Data.Item) -> Unit,
    data: HomeState.Data.Item
) {
    val shape = MaterialTheme.shapes.large
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = MaterialTheme.dimens.paddings.medium)
            .aspectRatio(2f)
            .shadow(elevation = MaterialTheme.dimens.elevation.medium, shape = shape)
            .background(
                color = MaterialTheme.colors.surface,
                shape = shape
            )
            .clickable { onItemClick(data) },
        contentAlignment = Alignment.Center
    ) {
        AsyncImage(
            model = data.imageUrl,
            modifier = Modifier.fillMaxSize(),
            contentDescription = "",
            contentScale = ContentScale.Crop
        )
    }
}
