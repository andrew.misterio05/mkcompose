package andrew.misterio.feature_dota2

import andrew.misterio.common.LoadModules
import andrew.misterio.common.navigation.Screens
import andrew.misterio.ui_invoker_trainer.ui.InvokerTrainerComposable
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.navigation.NavController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController

@Composable
fun FeatureDota2Composable(navController: NavController) {
    val localNavController = rememberNavController()
    LoadModules(modules)
    MaterialTheme(colors = colors) {
        NavHost(
            navController = localNavController,
            startDestination = Screens.Dota2.InvokerTrainer.route
        ) {
            composable(Screens.Dota2.InvokerTrainer.route) {
                InvokerTrainerComposable(localNavController)
            }
        }
    }
}
