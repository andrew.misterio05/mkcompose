package andrew.misterio.feature_dota2

import androidx.compose.material.Colors
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

internal val colors: Colors
    @Composable get() = MaterialTheme.colors.copy(
    primary = Color(0xFFFFA500),
    secondary = Color(0xFFF3F7F3)
)
