package andrew.misterio.ui_dota2.characters.data

import andrew.misterio05.domain_dota2.characters.GetCharactersInteractor

class Dota2CharactersProvider(
    private val getCharactersInteractor: GetCharactersInteractor,
) {

    suspend operator fun <T> invoke(map: (String, String, String) -> T): List<T> = getCharactersInteractor
        .invoke()
        .map { map(it.id, it.imageUrl, it.name) }
}
