package andrew.misterio.feature_dota2

import andrew.misterio.ui_invoker_trainer.invokerTrainerModule

val modules = listOf(
    invokerTrainerModule
)