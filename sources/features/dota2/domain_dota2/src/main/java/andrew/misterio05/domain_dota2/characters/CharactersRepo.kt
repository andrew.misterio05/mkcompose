package andrew.misterio05.domain_dota2.characters

interface CharactersRepo {
    suspend fun load(): List<Character>
}