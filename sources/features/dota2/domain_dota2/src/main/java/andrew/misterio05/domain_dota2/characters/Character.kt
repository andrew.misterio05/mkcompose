package andrew.misterio05.domain_dota2.characters

data class Character(
    val id: String,
    val imageUrl: String,
    val name: String,
)
