package andrew.misterio05.domain_dota2.characters

class GetCharactersInteractor(
    private val charactersRepo: CharactersRepo,
) {
    suspend operator fun invoke(): List<Character> = charactersRepo.load()
}