package andrew.misterio.domain_invoker_trainer.models

enum class SpellSlot {
    FIRST, SECOND, THIRD, ULTIMATE, EXT_FIRST, EXT_SECOND
}