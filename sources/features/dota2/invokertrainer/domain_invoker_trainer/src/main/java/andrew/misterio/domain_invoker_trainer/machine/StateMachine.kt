package andrew.misterio.domain_invoker_trainer.machine

interface StateMachine<EventT : Event, StateT : State> {
    val state: StateT
    suspend fun execute(event: EventT): StateT
}
