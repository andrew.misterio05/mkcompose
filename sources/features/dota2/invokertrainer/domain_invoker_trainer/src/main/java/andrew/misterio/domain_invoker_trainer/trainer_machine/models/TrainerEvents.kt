package andrew.misterio.domain_invoker_trainer.trainer_machine.models

import andrew.misterio.domain_invoker_trainer.machine.Event
import andrew.misterio.domain_invoker_trainer.models.SpellSlot

sealed class TrainerEvents: Event {
    data class CastBySlot(val slot: SpellSlot): TrainerEvents()
}
