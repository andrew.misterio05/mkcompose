package andrew.misterio.domain_invoker_trainer.trainer_machine.delegates

import andrew.misterio.domain_invoker_trainer.models.Spell
import andrew.misterio.domain_invoker_trainer.models.SpellSlot
import andrew.misterio.domain_invoker_trainer.models.Sphere
import andrew.misterio.domain_invoker_trainer.trainer_machine.models.TrainerState

class UltimateCastDelegate {
    operator fun invoke(
        state: TrainerState
    ): TrainerState = when (val newSpell = state.activatedSpheres.toSpell()) {
        state.slots.getSpell(SpellSlot.EXT_FIRST), Spell.EMPTY -> state
        else -> state.copy(
            slots = state.slots.copy(
                extSecond = state.slots.extFirst,
                extFirst = newSpell
            )
        )
    }

    private fun List<Sphere>.toSpell(): Spell = when {
        count(Sphere.QUAS::equals) == 3 -> Spell.COLD

        count(Sphere.QUAS::equals) == 2 &&
            count(Sphere.WEX::equals) == 1 -> Spell.GHOST

        count(Sphere.QUAS::equals) == 1 &&
            count(Sphere.WEX::equals) == 2 -> Spell.TORNADO

        count(Sphere.QUAS::equals) == 2 &&
            count(Sphere.EXORT::equals) == 1 -> Spell.WALL

        count(Sphere.QUAS::equals) == 1 &&
            count(Sphere.EXORT::equals) == 2 -> Spell.FORGE

        count(Sphere.WEX::equals) == 3 -> Spell.EMP

        count(Sphere.WEX::equals) == 2 &&
            count(Sphere.EXORT::equals) == 1 -> Spell.ALACRITY

        count(Sphere.WEX::equals) == 1 &&
            count(Sphere.EXORT::equals) == 2 -> Spell.METEOR

        count(Sphere.EXORT::equals) == 3 -> Spell.SUN

        count(Sphere.QUAS::equals) == 1 &&
            count(Sphere.WEX::equals) == 1 &&
            count(Sphere.EXORT::equals) == 1 -> Spell.BLAST

        else -> Spell.EMPTY
    }
}
