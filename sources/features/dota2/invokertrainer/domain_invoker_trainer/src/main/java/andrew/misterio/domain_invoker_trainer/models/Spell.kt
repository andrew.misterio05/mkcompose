package andrew.misterio.domain_invoker_trainer.models

enum class Spell {
    QUAS, WEX, EXORT, INVOKE,

    COLD,       // QQQ
    GHOST,      // QQW
    WALL,      // QQE
    FORGE,      // QEE
    TORNADO,    // QWW
    EMP,        // WWW
    ALACRITY,   // WWE
    METEOR,     // WEE
    SUN,        // EEE
    BLAST,       // QWE

    EMPTY
}