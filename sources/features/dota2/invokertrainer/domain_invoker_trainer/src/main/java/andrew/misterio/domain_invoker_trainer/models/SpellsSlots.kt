package andrew.misterio.domain_invoker_trainer.models

data class SpellsSlots(
    val extFirst: Spell = Spell.EMPTY,
    val extSecond: Spell = Spell.EMPTY
) {
    fun getSpell(slot: SpellSlot): Spell = when(slot) {
        SpellSlot.FIRST -> Spell.QUAS
        SpellSlot.SECOND -> Spell.WEX
        SpellSlot.THIRD -> Spell.EXORT
        SpellSlot.ULTIMATE -> Spell.INVOKE
        SpellSlot.EXT_FIRST -> extFirst
        SpellSlot.EXT_SECOND -> extSecond
    }
}
