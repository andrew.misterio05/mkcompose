package andrew.misterio.domain_invoker_trainer.trainer_machine

import andrew.misterio.domain_invoker_trainer.machine.StateMachine
import andrew.misterio.domain_invoker_trainer.models.Spell
import andrew.misterio.domain_invoker_trainer.models.SpellSlot
import andrew.misterio.domain_invoker_trainer.models.SpellsSlots
import andrew.misterio.domain_invoker_trainer.trainer_machine.delegates.GameStatusDelegate
import andrew.misterio.domain_invoker_trainer.trainer_machine.delegates.PrimaryCastDelegate
import andrew.misterio.domain_invoker_trainer.trainer_machine.delegates.SphereCastDelegate
import andrew.misterio.domain_invoker_trainer.trainer_machine.delegates.UltimateCastDelegate
import andrew.misterio.domain_invoker_trainer.trainer_machine.models.TrainerEvents
import andrew.misterio.domain_invoker_trainer.trainer_machine.models.TrainerState

class TrainerStateMachine(
    private val sphereCastDelegate: SphereCastDelegate,
    private val ultimateCastDelegate: UltimateCastDelegate,
    private val primaryCastDelegate: PrimaryCastDelegate,
    private val gameStatusDelegate: GameStatusDelegate,
) : StateMachine<TrainerEvents, TrainerState> {

    override var state: TrainerState = TrainerState()

    override suspend fun execute(event: TrainerEvents): TrainerState {
        state = when (event) {
            is TrainerEvents.CastBySlot -> when (event.slot) {
                SpellSlot.FIRST, SpellSlot.SECOND, SpellSlot.THIRD -> sphereCastDelegate(
                    state = state,
                    slot = event.slot
                )
                SpellSlot.ULTIMATE -> ultimateCastDelegate(state)
                SpellSlot.EXT_FIRST,
                SpellSlot.EXT_SECOND -> primaryCastDelegate(
                    state = state,
                    spell = state.slots.getSpell(event.slot)
                )
            }
        }
            .let(gameStatusDelegate::invoke)
        return state
    }
}
