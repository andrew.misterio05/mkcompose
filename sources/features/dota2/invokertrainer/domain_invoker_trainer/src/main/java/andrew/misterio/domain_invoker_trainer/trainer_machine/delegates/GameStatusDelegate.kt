package andrew.misterio.domain_invoker_trainer.trainer_machine.delegates

import andrew.misterio.domain_invoker_trainer.trainer_machine.models.GameData
import andrew.misterio.domain_invoker_trainer.trainer_machine.models.TrainerState

class GameStatusDelegate {
    operator fun invoke(state: TrainerState): TrainerState = when {
        state.game.progress == GameData.Progress.Idle -> state.copy(
            game = state.game.copy(
                progress = GameData.Progress.InProgress
            )
        )
        state.game.targetSpells.isEmpty() && state.game.progress == GameData.Progress.InProgress -> state.copy(
            game = state.game.copy(
                progress = GameData.Progress.Success
            )
        )
        else -> state
    }
}