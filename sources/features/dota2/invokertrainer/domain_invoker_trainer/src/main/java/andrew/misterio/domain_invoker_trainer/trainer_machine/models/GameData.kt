package andrew.misterio.domain_invoker_trainer.trainer_machine.models

import andrew.misterio.domain_invoker_trainer.models.Spell

data class GameData(
    val targetSpells: List<Spell> = listOf(
        Spell.BLAST,
        Spell.METEOR,
        Spell.WALL,
        Spell.EMP,
        Spell.FORGE,
        Spell.COLD,
        Spell.SUN
    ),
    val progress: Progress = Progress.Idle
) {
    sealed class Progress {
        object Idle : Progress()
        object Success : Progress()
        object Failed : Progress()
        object InProgress : Progress()
    }
}
