package andrew.misterio.domain_invoker_trainer.models

enum class Sphere {
    QUAS, WEX, EXORT
}