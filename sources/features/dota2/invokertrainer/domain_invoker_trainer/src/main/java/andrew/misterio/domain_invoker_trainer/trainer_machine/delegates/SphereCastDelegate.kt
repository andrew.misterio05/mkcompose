package andrew.misterio.domain_invoker_trainer.trainer_machine.delegates

import andrew.misterio.domain_invoker_trainer.models.Spell
import andrew.misterio.domain_invoker_trainer.models.SpellSlot
import andrew.misterio.domain_invoker_trainer.models.Sphere
import andrew.misterio.domain_invoker_trainer.trainer_machine.models.TrainerState

class SphereCastDelegate {

    operator fun invoke(
        state: TrainerState,
        slot: SpellSlot
    ): TrainerState = when (val sphere = state.slots.getSpell(slot).toSphere()) {
        null -> state
        else -> state.copy(
            activatedSpheres = state.activatedSpheres addNewSphere sphere
        )
    }

    private fun Spell.toSphere(): Sphere? = when (this) {
        Spell.QUAS -> Sphere.QUAS
        Spell.WEX -> Sphere.WEX
        Spell.EXORT -> Sphere.EXORT
        else -> null
    }

    private infix fun List<Sphere>.addNewSphere(sphere: Sphere?): List<Sphere> = when (sphere) {
        null -> this
        else -> toMutableList()
            .apply { add(0, sphere) }
            .take(3)
    }
}