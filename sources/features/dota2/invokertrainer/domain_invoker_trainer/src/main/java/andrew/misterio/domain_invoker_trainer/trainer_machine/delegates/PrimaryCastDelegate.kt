package andrew.misterio.domain_invoker_trainer.trainer_machine.delegates

import andrew.misterio.domain_invoker_trainer.models.Spell
import andrew.misterio.domain_invoker_trainer.trainer_machine.models.GameData
import andrew.misterio.domain_invoker_trainer.trainer_machine.models.TrainerState

class PrimaryCastDelegate {
    // TODO Нужно обновлять не только при нажатии основных спелов
    operator fun invoke(state: TrainerState, spell: Spell): TrainerState = state.game
        .targetSpells
        .let { target ->
            when {
                target.isNotEmpty() && target.first() == spell -> {
                    state.copy(
                        game = state.game.copy(
                            targetSpells = target.takeLast(target.size - 1)
                        )
                    )
                }
                else -> state
            }
        }
}
