package andrew.misterio.domain_invoker_trainer.trainer_machine.models

import andrew.misterio.domain_invoker_trainer.machine.State
import andrew.misterio.domain_invoker_trainer.models.Spell
import andrew.misterio.domain_invoker_trainer.models.SpellsSlots
import andrew.misterio.domain_invoker_trainer.models.Sphere

data class TrainerState(
    val slots: SpellsSlots = SpellsSlots(
        extFirst = Spell.EMPTY,
        extSecond = Spell.EMPTY
    ),
    val activatedSpheres: List<Sphere> = emptyList(),
    val game: GameData = GameData()
) : State
