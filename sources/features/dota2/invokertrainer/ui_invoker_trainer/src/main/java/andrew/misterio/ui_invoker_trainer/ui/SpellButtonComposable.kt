package andrew.misterio.ui_invoker_trainer.ui

import android.view.MotionEvent
import androidx.annotation.DrawableRes
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.input.pointer.pointerInteropFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.max

@OptIn(ExperimentalComposeUiApi::class)
@Composable
internal fun SpellButton(
    modifier: Modifier = Modifier,
    onClickDown: (() -> Unit)? = null,
    elevationIdle: Dp = 4.dp,
    elevationPressed: Dp = 1.dp,
    @DrawableRes resId: Int
) {
    var elevation by remember { mutableStateOf(elevationIdle) }
    Card(
        modifier = modifier
            .padding(max(elevationIdle, elevationPressed) - elevation / 3)
            .shadow(
                elevation = elevation,
                shape = MaterialTheme.shapes.medium
            )
            .padding(elevationPressed)
            .run {
                when (onClickDown) {
                    null -> this
                    else -> pointerInteropFilter {
                        when (it.action) {
                            MotionEvent.ACTION_DOWN -> {
                                elevation = elevationPressed
                                onClickDown.invoke()
                            }
                            MotionEvent.ACTION_UP -> elevation = elevationIdle
                        }
                        true
                    }
                }
            },
        border = BorderStroke(BORDER_WIDTH.dp, MaterialTheme.colors.primary)
    ) {
        Image(
            painter = painterResource(id = resId),
            contentDescription = resId.toString()
        )
    }
}
