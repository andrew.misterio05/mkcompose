package andrew.misterio.ui_invoker_trainer.models

import andrew.misterio.domain_invoker_trainer.models.Spell
import andrew.misterio.ui_invoker_trainer.R
import androidx.annotation.DrawableRes

data class SpellViewData(
    @DrawableRes val imageRes: Int,
    val id: Spell
) {
    companion object {
        val EMPTY = SpellViewData(
            imageRes = R.drawable.ic_spell_background,
            Spell.EMPTY
        )
    }
}
