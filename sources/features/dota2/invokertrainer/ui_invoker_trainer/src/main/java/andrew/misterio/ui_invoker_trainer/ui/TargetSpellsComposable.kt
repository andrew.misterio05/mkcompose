package andrew.misterio.ui_invoker_trainer.ui

import andrew.misterio.common.composable.square
import andrew.misterio.ui_invoker_trainer.models.TargetViewData
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun TargetSpells(
    modifier: Modifier = Modifier,
    targetSpells: List<TargetViewData>
) = Column(
    modifier = modifier,
    verticalArrangement = Arrangement.Center
) {

    targetSpells.forEachIndexed { index, item ->
        val scale = ((index + 1f) / targetSpells.size) / 2f
        SpellButton(
            modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .fillMaxWidth(scale)
                .square(),
            resId = item.data.imageRes
        )
    }
}
