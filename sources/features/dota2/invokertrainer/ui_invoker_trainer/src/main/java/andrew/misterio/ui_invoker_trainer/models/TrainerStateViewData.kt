package andrew.misterio.ui_invoker_trainer.models

import andrew.misterio.domain_invoker_trainer.models.SpellSlot

data class TrainerStateViewData(
    private val slots: Map<SpellSlot, SpellViewData>
) {
    fun getSpellBySlot(slot: SpellSlot): SpellViewData = slots[slot] ?: SpellViewData.EMPTY
}
