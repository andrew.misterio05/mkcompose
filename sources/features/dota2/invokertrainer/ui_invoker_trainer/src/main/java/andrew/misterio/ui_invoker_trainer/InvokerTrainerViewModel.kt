package andrew.misterio.ui_invoker_trainer

import andrew.misterio.common.viewmodel.coroutine
import andrew.misterio.domain_invoker_trainer.models.SpellSlot
import andrew.misterio.domain_invoker_trainer.trainer_machine.models.TrainerEvents
import andrew.misterio.domain_invoker_trainer.trainer_machine.models.TrainerState
import andrew.misterio.domain_invoker_trainer.trainer_machine.TrainerStateMachine
import andrew.misterio.ui_invoker_trainer.mappers.TrainerStateMapper
import andrew.misterio.ui_invoker_trainer.models.SpellViewData
import andrew.misterio.ui_invoker_trainer.models.SphereViewData
import andrew.misterio.ui_invoker_trainer.models.TargetViewData
import andrew.misterio.ui_invoker_trainer.models.TrainerStateViewData
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

interface InvokerTrainerViewModel {
    val stateData: LiveData<TrainerStateViewData>
    val spheres: LiveData<List<SphereViewData>>
    val targets: LiveData<List<TargetViewData>>
    fun onSlotClick(slot: SpellSlot)
}

class InvokerTrainerViewModelImpl(
    private val stateMachine: TrainerStateMachine,
    private val mapper: TrainerStateMapper
) : ViewModel(), InvokerTrainerViewModel {
    override val stateData = MutableLiveData<TrainerStateViewData>()
    override val spheres = MutableLiveData<List<SphereViewData>>()
    override val targets = MutableLiveData<List<TargetViewData>>()

    init {
        updateViewDataState(state = stateMachine.state)
    }

    override fun onSlotClick(slot: SpellSlot) {
        coroutine {
            updateViewDataState(
                oldState = stateMachine.state,
                state = stateMachine.execute(TrainerEvents.CastBySlot(slot))
            )
        }
    }
// FIXME После посещения экрана МК тут начинаются глюки лютые
    private fun updateViewDataState(oldState: TrainerState? = null, state: TrainerState) {
        if (oldState?.slots != state.slots) {
            stateData.value = mapper.mapState(state.slots)
        }

        if (oldState?.game?.targetSpells != state.game.targetSpells) {
            targets.value = mapper.mapTargets(
                oldList = targets.value.orEmpty(),
                spells = state.game.targetSpells
            )
        }

        if (oldState?.activatedSpheres != state.activatedSpheres) {
            spheres.value = mapper.mapSpheres(state.activatedSpheres)
        }
    }
}
