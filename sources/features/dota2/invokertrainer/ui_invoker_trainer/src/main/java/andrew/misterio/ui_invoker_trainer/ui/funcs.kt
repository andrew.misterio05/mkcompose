package andrew.misterio.ui_invoker_trainer.ui

import andrew.misterio.domain_invoker_trainer.models.SpellSlot
import andrew.misterio.ui_invoker_trainer.models.SpellViewData
import andrew.misterio.ui_invoker_trainer.models.TrainerStateViewData
import android.content.res.Configuration
import androidx.annotation.DrawableRes
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalConfiguration

val isLandscape: Boolean
    @Composable get() = LocalConfiguration.current.orientation ==
        Configuration.ORIENTATION_LANDSCAPE

@DrawableRes
fun TrainerStateViewData?.getImageRes(slot: SpellSlot): Int = (this?.getSpellBySlot(slot)
    ?: SpellViewData.EMPTY)
    .imageRes
