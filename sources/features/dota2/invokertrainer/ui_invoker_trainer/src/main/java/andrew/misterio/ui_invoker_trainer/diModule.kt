package andrew.misterio.ui_invoker_trainer

import andrew.misterio.domain_invoker_trainer.trainer_machine.delegates.SphereCastDelegate
import andrew.misterio.domain_invoker_trainer.trainer_machine.TrainerStateMachine
import andrew.misterio.domain_invoker_trainer.trainer_machine.delegates.GameStatusDelegate
import andrew.misterio.domain_invoker_trainer.trainer_machine.delegates.PrimaryCastDelegate
import andrew.misterio.domain_invoker_trainer.trainer_machine.delegates.UltimateCastDelegate
import andrew.misterio.ui_invoker_trainer.mappers.TrainerStateMapper
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val invokerTrainerModule = module {
    factory { SphereCastDelegate() }
    factory { UltimateCastDelegate() }
    factory { PrimaryCastDelegate() }
    factory { GameStatusDelegate() }
    factory { TrainerStateMachine(get(), get(), get(), get()) }
    factory { TrainerStateMapper() }
    viewModel {
        InvokerTrainerViewModelImpl(
            stateMachine = get(),
            mapper = get(),
        )
    }
}
