package andrew.misterio.ui_invoker_trainer.mappers

import andrew.misterio.domain_invoker_trainer.models.Spell
import andrew.misterio.domain_invoker_trainer.models.SpellSlot
import andrew.misterio.domain_invoker_trainer.models.SpellsSlots
import andrew.misterio.domain_invoker_trainer.models.Sphere
import andrew.misterio.ui_invoker_trainer.R
import andrew.misterio.ui_invoker_trainer.models.SpellViewData
import andrew.misterio.ui_invoker_trainer.models.SphereViewData
import andrew.misterio.ui_invoker_trainer.models.TargetViewData
import andrew.misterio.ui_invoker_trainer.models.TrainerStateViewData
import arrow.core.andThen

class TrainerStateMapper {

    fun mapState(slots: SpellsSlots) = TrainerStateViewData(
        SpellSlot.values().associateWith(slots::getSpell andThen ::mapSpell)
    )

    fun mapSpheres(spheres: List<Sphere>): List<SphereViewData> = spheres
        .map(::getSphereRes andThen ::SphereViewData)

    fun mapTargets(
        oldList: List<TargetViewData>,
        spells: List<Spell>
    ): List<TargetViewData> = when {
        oldList.isNotEmpty() &&
            oldList.size > spells.size -> {
            oldList.take(oldList.size - 1)
        }
        else -> {
            spells.map(::mapSpell)
                .map(::TargetViewData)
                .asReversed()
        }
    }
        .takeLast(4)

    private fun mapSpell(spell: Spell) = SpellViewData(
        imageRes = getSpellRes(spell),
        id = spell
    )

    private fun getSpellRes(spell: Spell) = when (spell) {
        Spell.QUAS -> R.drawable.ic_quas
        Spell.WEX -> R.drawable.ic_wex
        Spell.EXORT -> R.drawable.ic_exort
        Spell.INVOKE -> R.drawable.ic_invoke
        Spell.COLD -> R.drawable.ic_cold_snap
        Spell.GHOST -> R.drawable.ic_ghost_walk
        Spell.TORNADO -> R.drawable.ic_tornado
        Spell.EMP -> R.drawable.ic_emp
        Spell.ALACRITY -> R.drawable.ic_alacrity
        Spell.METEOR -> R.drawable.ic_chaos_meteor
        Spell.SUN -> R.drawable.ic_sun_strike
        Spell.BLAST -> R.drawable.ic_deafening_blast
        Spell.FORGE -> R.drawable.ic_forge_spirit
        Spell.WALL -> R.drawable.ic_ice_wall
        Spell.EMPTY -> R.drawable.ic_spell_background
    }

    private fun getSphereRes(sphere: Sphere) = when (sphere) {
        Sphere.QUAS -> R.drawable.ic_quas
        Sphere.WEX -> R.drawable.ic_wex
        Sphere.EXORT -> R.drawable.ic_exort
    }
}
