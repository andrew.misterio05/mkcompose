package andrew.misterio.ui_invoker_trainer.models

import androidx.annotation.DrawableRes

data class SphereViewData(@DrawableRes val imageRes: Int)
