package andrew.misterio.ui_invoker_trainer.ui

import andrew.misterio.common.screenWidth
import andrew.misterio.domain_invoker_trainer.models.SpellSlot
import andrew.misterio.ui_invoker_trainer.InvokerTrainerViewModel
import andrew.misterio.ui_invoker_trainer.InvokerTrainerViewModelImpl
import andrew.misterio.ui_invoker_trainer.models.SphereViewData
import andrew.misterio.ui_invoker_trainer.models.TrainerStateViewData
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import andrew.misterio.common.getViewModel

@Composable
fun InvokerTrainerComposable(navController: NavController) = Box(
    modifier = Modifier.fillMaxSize()
) {
    val vm: InvokerTrainerViewModel = getViewModel<InvokerTrainerViewModelImpl>()
    val state by vm.stateData.observeAsState()
    val spheres by vm.spheres.observeAsState()
    val target by vm.targets.observeAsState()

    Column(modifier = Modifier.align(Alignment.BottomCenter)) {
        val isLandscape = isLandscape
        val spellButtonSize: Dp = when (isLandscape) {
            true -> screenWidth / 6
            else -> screenWidth / 4
        }

        TargetSpells(
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f)
                .align(Alignment.CenterHorizontally),
            targetSpells = target.orEmpty()
        )

        SpheresStatusComposable(
            modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .height(spellButtonSize / 2),
            activatedSpheres = spheres.orEmpty()
        )

        ButtonsComposable(
            state = state,
            isLandscape = isLandscape,
            spellButtonSize = spellButtonSize,
            onClick = vm::onSlotClick
        )
    }
}

@Composable
private fun ButtonsComposable(
    state: TrainerStateViewData?,
    isLandscape: Boolean,
    spellButtonSize: Dp,
    onClick: (SpellSlot) -> Unit
) = when {
    isLandscape -> DrawButtonsLandscape(
        state = state,
        onClick = onClick,
        buttonSize = spellButtonSize
    )
    else -> DrawButtonsPortrait(
        state = state,
        onClick = onClick,
        buttonSize = spellButtonSize
    )
}


@Composable
private fun SpheresStatusComposable(
    modifier: Modifier = Modifier,
    activatedSpheres: List<SphereViewData>
) = Row(modifier = modifier) {
    activatedSpheres.forEach { sphere ->
        Image(
            modifier = Modifier
                .fillMaxHeight()
                .aspectRatio(1f)
                .padding(4.dp)
                .clip(CircleShape)
                .border(
                    color = MaterialTheme.colors.primary,
                    shape = CircleShape,
                    width = BORDER_WIDTH.dp
                ),
            painter = painterResource(id = sphere.imageRes),
            contentDescription = "${sphere.imageRes}"
        )
    }
}

@Composable
private fun DrawButtonsLandscape(
    state: TrainerStateViewData?,
    buttonSize: Dp,
    onClick: (SpellSlot) -> Unit
) = Row {
    state.SpellButton(SpellSlot.FIRST, buttonSize, onClick)
    state.SpellButton(SpellSlot.SECOND, buttonSize, onClick)
    state.SpellButton(SpellSlot.THIRD, buttonSize, onClick)
    state.SpellButton(SpellSlot.EXT_FIRST, buttonSize, onClick)
    state.SpellButton(SpellSlot.EXT_SECOND, buttonSize, onClick)
    state.SpellButton(SpellSlot.ULTIMATE, buttonSize, onClick)
}

@Composable
private fun DrawButtonsPortrait(
    state: TrainerStateViewData?,
    buttonSize: Dp,
    onClick: (SpellSlot) -> Unit
) {
    Row {
        state.SpellButton(SpellSlot.FIRST, buttonSize, onClick)
        state.SpellButton(SpellSlot.SECOND, buttonSize, onClick)
        state.SpellButton(SpellSlot.THIRD, buttonSize, onClick)
        state.SpellButton(SpellSlot.ULTIMATE, buttonSize, onClick)
    }
    Row(
        horizontalArrangement = Arrangement.End,
        modifier = Modifier
            .padding(end = buttonSize / 2)
            .fillMaxWidth()
    ) {
        state.SpellButton(SpellSlot.EXT_FIRST, buttonSize, onClick)
        state.SpellButton(SpellSlot.EXT_SECOND, buttonSize, onClick)
    }
}

@Composable
fun TrainerStateViewData?.SpellButton(
    slot: SpellSlot,
    size: Dp,
    onClick: (SpellSlot) -> Unit
) = SpellButton(
    modifier = Modifier
        .width(size)
        .aspectRatio(1f),
    resId = getImageRes(slot),
    onClickDown = { onClick(slot) }
)
