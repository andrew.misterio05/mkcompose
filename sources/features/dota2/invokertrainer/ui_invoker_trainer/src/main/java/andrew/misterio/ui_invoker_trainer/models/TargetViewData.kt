package andrew.misterio.ui_invoker_trainer.models

data class TargetViewData(
    val data: SpellViewData
)
