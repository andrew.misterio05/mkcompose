package andrew.misterio05.data_mortalkombat.models

import com.google.gson.annotations.SerializedName

data class CharacterData(
    @SerializedName("name")
    val name: Text,

    @SerializedName("basic")
    val basic: List<AttackOut>,
    @SerializedName("combos")
    val combos: List<AttackOut>,
    @SerializedName("specials")
    val specials: List<AttackOut>,

    @SerializedName("endings")
    val endings: Endings,

    @SerializedName("combinations")
    val combinations: List<CombinationOut>,

    @SerializedName("order")
    val order: Int,
    // Можно позже заполнить руками в firebase
    @SerializedName("styles")
    val optional: List<StyleOption?>?,
) {

    data class StyleOption(
        @SerializedName("category")
        val category: Category,
        @SerializedName("id")
        val id: Int,
        @SerializedName("incompatibles")
        val incompatible: List<StyleOption>,
    ) {

        enum class Category {
            BASIC, COMBO, SPECIAL;
        }
    }

    data class Text(
        val ru: String,
        val en: String,
    )

    data class CombinationOut(
        @SerializedName("buttons") val buttons: String,
        @SerializedName("damage") val damage: String,
        @SerializedName("style") val style: Text,
        @SerializedName("position") val position: Text,
        @SerializedName("description") val description: Text,
    )

    data class Frames(
        @SerializedName("start") val start: Int,
        @SerializedName("active") val active: Int,
        @SerializedName("recovery") val recovery: Int,
        @SerializedName("punchForCancel") val punchForCancel: Int,
        @SerializedName("punchForHit") val punchForHit: Int,
        @SerializedName("punchForBlock") val punchForBlock: Int,
    )

    data class Damage(
        @SerializedName("value")
        val value: Float,
        @SerializedName("inBlock")
        val inBlock: Float,
    )

    data class Endings(
        @SerializedName("fatalities")
        val fatalities: List<Fatality>,
        @SerializedName("brutalities")
        val brutalities: List<Brutality>,
    )

    data class Fatality(
        @SerializedName("name") val name: Text,
        @SerializedName("buttons") val buttons: String,
    )

    data class Brutality(
        @SerializedName("name") val name: Text,
        @SerializedName("buttons") val buttons: String,
        @SerializedName("description") val description: Text,
    )

    data class AttackOut(
        @SerializedName("id") val id: Int,
        @SerializedName("name") val name: Text,
        @SerializedName("buttons") val buttons: String,
        @SerializedName("direction") val direction: Text?,
        @SerializedName("damage") val damage: Damage,
        @SerializedName("frames") val frames: Frames,
        @SerializedName("crushing_blow") val crushingBlow: Text?,
        @SerializedName("parentId") val parentId: Int?,
    )
}
