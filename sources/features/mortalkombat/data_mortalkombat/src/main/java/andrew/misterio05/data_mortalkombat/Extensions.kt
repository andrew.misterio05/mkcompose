package andrew.misterio05.data_mortalkombat

internal const val IMAGE_FILE_ENDING = ".webp"

fun createImagePath(characterId: String): String = "images/${characterId}$IMAGE_FILE_ENDING"
