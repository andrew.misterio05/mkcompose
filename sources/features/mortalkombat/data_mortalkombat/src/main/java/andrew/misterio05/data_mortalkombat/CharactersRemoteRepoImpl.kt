package andrew.misterio05.data_mortalkombat

import andrew.misterio05.domain_mortalkombat.characters.Character
import andrew.misterio05.domain_mortalkombat.characters.CharactersRemoteRepo

class CharactersRemoteRepoImpl(
    private val dataStorage: DataStorage,
) : CharactersRemoteRepo {
    override suspend fun getList(): List<Character> = dataStorage.getCharacters()
        ?.mapNotNull {
            it.key?.to(it.value ?: return@mapNotNull null)
        }
        ?.toMap()
        .orEmpty()
        .map { Character(it.key, createImagePath(it.key), it.value.name.ru) }
}
