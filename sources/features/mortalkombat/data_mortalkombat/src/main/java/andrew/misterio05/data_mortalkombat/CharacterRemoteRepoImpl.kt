package andrew.misterio05.data_mortalkombat

import andrew.misterio05.data_mortalkombat.models.CharacterData
import andrew.misterio05.domain_mortalkombat.character.CharacterRemoteRepo
import andrew.misterio05.domain_mortalkombat.character.models.Character
import andrew.misterio05.domain_mortalkombat.character.models.FrameData
import andrew.misterio05.domain_mortalkombat.character.models.MoveData
import andrew.misterio05.domain_mortalkombat.character.models.MoveDescription
import andrew.misterio05.domain_mortalkombat.character.models.MoveType
import andrew.misterio05.domain_mortalkombat.character.models.Section
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class CharacterRemoteRepoImpl(
    private val dataStorage: DataStorage
) : CharacterRemoteRepo {
    override suspend fun getCharacter(id: String): Character = withContext(Dispatchers.IO) {
        dataStorage.getCharacters()?.get(id)?.let {
            Character(
                name = it.name.ru,
                id = id,
                data = mapOf(
                    Section.BASIC to it.basic.map(::mapMoveData),
                    Section.COMBOS to it.combos.map(::mapMoveData),
                    Section.SPECIALS to it.specials.map(::mapMoveData),
                    Section.BRUTALITIES to it.endings.brutalities.map { brutality ->
                        MoveData(
                            input = brutality.buttons,
                            description = MoveDescription.Text(
                                name = brutality.name.ru,
                                value = brutality.description.ru,
                            )
                        )
                    },
                    Section.FATALITIES to it.endings.fatalities.map { fatality ->
                        MoveData(
                            input = fatality.buttons,
                            description = MoveDescription.Text(
                                name = fatality.name.ru,
                                value = "",
                            )
                        )
                    },
                    Section.COMBO_STRING to it.combinations.map { combination ->
                        MoveData(
                            input = combination.buttons,
                            description = MoveDescription.Combo(
                                damage = parseComboDamage(combination.damage),
                                tinyDescription = combination.position.ru,
                            )
                        )
                    },
                )
            )
        }!!
    }

    private fun mapMoveData(attack: CharacterData.AttackOut) =
        MoveData(
            input = attack.buttons,
            description = mapAttack(attack)
        )

    private fun mapAttack(attack: CharacterData.AttackOut) =
        MoveDescription.Attack(
            name = attack.name.ru,
            frameData = FrameData(
                startUp = attack.frames.start,
                active = attack.frames.active,
                recovery = attack.frames.recovery,
                cancelAdvantage = attack.frames.punchForCancel,
                hitAdvantage = attack.frames.punchForHit,
                blockAdvantage = attack.frames.punchForBlock,
                flawlessBlockAdvantage = 0
            ),
            type = mapAttackType(attack),
            isExtra = attack.parentId != null
        )

    private fun mapAttackType(it: CharacterData.AttackOut) = when (it.direction?.ru?.trim()) {
        "Высокий" -> MoveType.HIGH
        "Средний" -> MoveType.MID
        "Оверхед" -> MoveType.OVERHEAD
        "Нижний" -> MoveType.LOW
        else -> MoveType.UNKNOWN
    }

    private fun parseComboDamage(damage: String): MoveDescription.Combo.Damage = when {
        damage.contains("-".toRegex()) -> damage.split("-".toRegex(), 2)
            .mapNotNull(String::toFloatOrNull)
            .let { damages ->
                MoveDescription.Combo.Damage.Range(
                    min = damages.getOrElse(0) { 0f },
                    max = damages.getOrElse(1) { 0f }
                )
            }
        else -> MoveDescription.Combo.Damage.Exact(damage.toFloatOrNull() ?: 0f)
    }
}
