package andrew.misterio05.data_mortalkombat

import andrew.misterio05.data_mortalkombat.models.CharacterData

interface DataStorage {
    suspend fun getCharacters(forceUpdate: Boolean = false): Map<String?, CharacterData?>?
}
