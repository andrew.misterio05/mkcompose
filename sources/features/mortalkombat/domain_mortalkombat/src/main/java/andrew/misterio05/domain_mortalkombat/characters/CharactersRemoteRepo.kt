package andrew.misterio05.domain_mortalkombat.characters

interface CharactersRemoteRepo {
    suspend fun getList(): List<Character>
}
