package andrew.misterio05.domain_mortalkombat.character.models

sealed class MoveDescription {
    data class Text(
        val name: String,
        val value: String
    ) : MoveDescription()

    data class Attack(
        val name: String,
        val frameData: FrameData,
        val type: MoveType,
        val isExtra: Boolean
    ) : MoveDescription()

    data class Combo(
        val damage: Damage,
        val tinyDescription: String,
    ) : MoveDescription() {
        sealed class Damage {
            data class Range(val min: Float, val max: Float) : Damage()
            data class Exact(val value: Float) : Damage()
        }
    }
}
