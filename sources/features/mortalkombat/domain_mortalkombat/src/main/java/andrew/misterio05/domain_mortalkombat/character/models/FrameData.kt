package andrew.misterio05.domain_mortalkombat.character.models

data class FrameData(
    val startUp: Int,
    val active: Int,
    val recovery: Int,

    val cancelAdvantage: Int,

    val hitAdvantage: Int,
    val blockAdvantage: Int,
    val flawlessBlockAdvantage: Int,
)
