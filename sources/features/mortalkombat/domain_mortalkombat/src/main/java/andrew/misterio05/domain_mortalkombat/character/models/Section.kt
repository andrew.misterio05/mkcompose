package andrew.misterio05.domain_mortalkombat.character.models

enum class Section {
    BASIC, COMBOS, SPECIALS, FATALITIES, BRUTALITIES, CRUSHING_BLOW, COMBO_STRING
}
