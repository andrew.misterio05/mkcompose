package andrew.misterio05.domain_mortalkombat.character.models

data class Character(
    val name: String,
    val id: String = name,
    val data: Map<Section, List<MoveData>>
)
