package andrew.misterio05.domain_mortalkombat.character.models

enum class MoveType {
    OVERHEAD, HIGH, MID, LOW, UNKNOWN
}
