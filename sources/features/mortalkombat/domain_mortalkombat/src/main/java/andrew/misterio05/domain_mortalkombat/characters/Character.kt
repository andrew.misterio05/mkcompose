package andrew.misterio05.domain_mortalkombat.characters

data class Character(
    val id: String,
    val imageUrl: String,
    val name: String,
)
