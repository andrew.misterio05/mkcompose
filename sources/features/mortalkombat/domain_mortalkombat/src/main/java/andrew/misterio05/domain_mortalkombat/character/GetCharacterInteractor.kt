package andrew.misterio05.domain_mortalkombat.character

import andrew.misterio05.domain_mortalkombat.character.models.Character

class GetCharacterInteractor(
    private val characterRemoteRepo: CharacterRemoteRepo
) {
    suspend fun execute(id: String): Character = characterRemoteRepo.getCharacter(id)
}
