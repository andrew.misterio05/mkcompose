package andrew.misterio05.domain_mortalkombat.character.models

data class MoveData(
    val input: String,
    val description: MoveDescription,
)
