package andrew.misterio05.domain_mortalkombat.character

import andrew.misterio05.domain_mortalkombat.character.models.Character

interface CharacterRemoteRepo {
    suspend fun getCharacter(id: String): Character
}
