package andrew.misterio05.ui_mortalkombat.platform

import andrew.misterio05.data_mortalkombat.models.CharacterData
import andrew.misterio05.data_mortalkombat.models.Data
import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.File

internal class LocaleFileStorageDelegate(
    private val gson: Gson,
    private val context: Context,
) {

    val file by lazy { File(context.cacheDir, FILE_NAME) }

    internal fun readData(): Data? = kotlin.runCatching {
        file.readText().let(::parseData)
    }
        .getOrNull()

    private fun parseData(text: String): Data? = gson.fromJson(
        text,
        object : TypeToken<Map<String?, CharacterData?>>() {}.type,
    )

    companion object {
        private const val FILE_NAME = "data.json"
    }
}
