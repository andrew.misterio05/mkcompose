package andrew.misterio05.ui_mortalkombat.platform

import andrew.misterio05.data_mortalkombat.DataStorage
import andrew.misterio05.data_mortalkombat.models.Data
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.ktx.storage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext
import java.io.File

class DataStorageImpl internal constructor(
    private val fileStorageDelegate: LocaleFileStorageDelegate,
    private val cacheStorageDelegate: CacheStorageDelegate,
) : DataStorage {

    private val db by lazy { Firebase.storage }

    override suspend fun getCharacters(
        forceUpdate: Boolean,
    ): Data? = withContext(Dispatchers.IO) {
        val localeFile = fileStorageDelegate.file
        val remoteFile = db.reference.child(MKPATH).child(DATA_FILE_NAME)

        val lasModifyRemote = remoteFile.metadata.await().updatedTimeMillis
        val lasModifyLocale = when {
            localeFile.exists() -> localeFile.lastModified()
            else -> 0L
        }
        val cache = cacheStorageDelegate.value

        return@withContext when {
            forceUpdate -> loadRemote(remoteFile, localeFile)
            else -> when {
                cache != null &&
                    cache.lastModify >= lasModifyRemote &&
                    cache.lastModify >= lasModifyLocale -> {
                    cache.data
                }
                lasModifyLocale >= lasModifyRemote -> {
                    readFile()
                }
                else -> {
                    loadRemote(remoteFile, localeFile)
                }
            }
        }
    }

    private fun readFile(): Data? = fileStorageDelegate
        .readData()
        .also(cacheStorageDelegate::save)

    private suspend fun loadRemote(remoteFile: StorageReference, localeFile: File): Data? {
        remoteFile.getFile(localeFile).await()
        return readFile()
    }

    companion object {
        private const val MKPATH = "mk11"
        private const val DATA_FILE_NAME = "data.json"
    }
}
