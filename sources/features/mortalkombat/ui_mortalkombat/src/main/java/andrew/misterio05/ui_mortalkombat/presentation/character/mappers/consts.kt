import andrew.misterio.design.list.SeparatorViewItem
import andrew.misterio05.ui_mortalkombat.R
import andrew.misterio05.ui_mortalkombat.presentation.character.PADDINGS

internal val separatorViewData = SeparatorViewItem(thickness = PADDINGS)

object PreparedButtons {
    const val AMPLIFY = 'A'

    const val FORWARD = 'F'
    const val BACK = 'B'
    const val UP = 'U'
    const val DOWN = 'D'

    const val FP = '1'
    const val BP = '2'
    const val FK = '3'
    const val BK = '4'

    const val BLOCK = 'B'
    const val INTERACT = 'I'
    const val STANCE = 'S'
    const val THROW = 'T'

    const val JUMP = 'J'
    const val CRUSHING = 'C'
}

object PlatformsButtons {
    private val COMMON = mapOf(
        PreparedButtons.AMPLIFY to R.drawable.ic_amplify,
        PreparedButtons.UP to R.drawable.ic_arrow_upward,
        PreparedButtons.DOWN to R.drawable.ic_arrow_downward,
        PreparedButtons.FORWARD to R.drawable.ic_arrow_forward,
        PreparedButtons.BACK to R.drawable.ic_arrow_back,
        PreparedButtons.CRUSHING to R.drawable.ic_cb,
        PreparedButtons.JUMP to R.drawable.ic_arrow_upward_forward,
        PreparedButtons.THROW to R.drawable.ic_throw,
        PreparedButtons.STANCE to R.drawable.ic_l2,
        PreparedButtons.INTERACT to R.drawable.ic_l1,
    )

    internal val XBOX = COMMON + mapOf(
        PreparedButtons.FP to R.drawable.ic_xboxone_x,
        PreparedButtons.BP to R.drawable.ic_xboxone_y,
        PreparedButtons.FK to R.drawable.ic_xboxone_a,
        PreparedButtons.BK to R.drawable.ic_xboxone_b,
    )

    internal val PS = COMMON + mapOf(
        PreparedButtons.FP to R.drawable.ic_ps4_square,
        PreparedButtons.BP to R.drawable.ic_ps4_triangle,
        PreparedButtons.FK to R.drawable.ic_ps4_cross,
        PreparedButtons.BK to R.drawable.ic_ps4_circle,
    )
}
