package andrew.misterio05.ui_mortalkombat.presentation.character.ui.screen

import andrew.misterio.design.list.DelegatedLazyList
import andrew.misterio.design.list.listDelegate
import andrew.misterio.design.list.separatorVerticalDelegate
import andrew.misterio05.ui_mortalkombat.presentation.character.PADDINGS
import andrew.misterio05.ui_mortalkombat.presentation.character.STROKE_WIDTH
import andrew.misterio05.ui_mortalkombat.presentation.character.models.ComboLineViewData
import andrew.misterio05.ui_mortalkombat.presentation.character.models.InlineTextViewData
import andrew.misterio05.ui_mortalkombat.presentation.character.models.ScreenViewData
import andrew.misterio05.ui_mortalkombat.presentation.character.ui.screen.buttons.Attack
import andrew.misterio05.ui_mortalkombat.presentation.character.ui.screen.buttons.Kombo
import andrew.misterio05.ui_mortalkombat.presentation.character.ui.screen.buttons.Special
import androidx.compose.animation.Crossfade
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.scale
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp

private val DELEGATES = mapOf(
    separatorVerticalDelegate(),
    listDelegate<ComboLineViewData>(ComboLineViewData) {
        DrawItem(comboLineViewData = it)
    }
)

@Composable
fun Content(
    modifier: Modifier,
    listState: LazyListState = rememberLazyListState(),
    screenViewData: ScreenViewData,
) = Crossfade(
    modifier = modifier
        .padding(PADDINGS)
        .fillMaxSize(),
    targetState = screenViewData.listState
) { state ->
    when (state) {
        is ScreenViewData.ListState.Data -> {
            DelegatedLazyList(
                items = state.combos,
                state = listState,
                delegates = DELEGATES,
            )
        }
        is ScreenViewData.ListState.Loading -> Box(modifier = Modifier.fillMaxSize()) {
            CircularProgressIndicator(
                modifier = Modifier.align(Alignment.Center),
                color = MaterialTheme.colors.onBackground
            )
        }
    }
}

@Composable
private fun DrawItem(
    comboLineViewData: ComboLineViewData,
) = when (val description = comboLineViewData.description) {
    is ComboLineViewData.Description.Attack -> DrawAttack(
        value = comboLineViewData.value,
        description = description,
    )
    is ComboLineViewData.Description.Kombo -> ButtonsDataLine {
        Kombo(
            value = comboLineViewData.value,
            moveDescription = description,
        )
    }
    is ComboLineViewData.Description.Special -> ButtonsDataLine {
        Special(
            value = comboLineViewData.value,
            moveDescription = description,
        )
    }
}

@Composable
private fun DrawAttack(
    value: InlineTextViewData,
    description: ComboLineViewData.Description.Attack,
) {
    val attack: @Composable () -> Unit = @Composable {
        ButtonsDataLine {
            Attack(
                value = value,
                moveDescription = description,
            )
        }
    }
    when (description.extension) {
        ComboLineViewData.Description.Attack.Extension.IS -> Row(
            modifier = Modifier.alpha(0.8f),
            horizontalArrangement = Arrangement.End,
        ) {
            Text(
                text = "  ",
                color = MaterialTheme.colors.primary,
                fontSize = INPUT_TEXT_SIZE,
                fontWeight = FontWeight.ExtraBold
            )
            Box(modifier = Modifier.scale(1f)) {
                attack()
            }
        }
        else -> attack()
    }
}

@Composable
private fun ButtonsDataLine(
    modifier: Modifier = Modifier,
    shape: Shape = MaterialTheme.shapes.medium,
    content: @Composable BoxScope.() -> Unit
) = Box(
    modifier = modifier
        .padding(start = 2.dp, end = 2.dp)
        .shadow(elevation = 2.dp, shape = shape)
        .padding(start = 1.dp, top = 1.dp, end = 1.dp, bottom = 2.dp)
        .background(color = MaterialTheme.colors.surface, shape = shape)
        .border(
            width = STROKE_WIDTH,
            color = MaterialTheme.colors.primary,
            shape = shape
        )
        .padding(4.dp)
        .fillMaxWidth(),
    content = content
)
