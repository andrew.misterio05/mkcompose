package andrew.misterio05.ui_mortalkombat.presentation.character.ui.screen.buttons

import andrew.misterio05.ui_mortalkombat.presentation.character.models.ComboLineViewData
import andrew.misterio05.ui_mortalkombat.presentation.character.models.InlineTextViewData
import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable

@Composable
internal fun Special(
    moveDescription: ComboLineViewData.Description.Special,
    value: InlineTextViewData,
) = Column {
    Buttons(value = value)
    Title(text = moveDescription.name)
}
