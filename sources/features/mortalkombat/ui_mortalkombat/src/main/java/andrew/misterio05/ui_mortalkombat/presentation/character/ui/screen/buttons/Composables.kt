package andrew.misterio05.ui_mortalkombat.presentation.character.ui.screen.buttons

import andrew.misterio05.ui_mortalkombat.presentation.character.models.InlineTextViewData
import andrew.misterio05.ui_mortalkombat.presentation.character.ui.screen.INPUT_TEXT_SIZE
import androidx.compose.foundation.text.InlineTextContent
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.unit.sp

@Composable
internal fun Buttons(
    value: InlineTextViewData,
    modifier: Modifier = Modifier,
) = Buttons(
    text = value.text,
    inlineContent = value.keyToInlineInfo,
    modifier = modifier
)
@Composable
internal fun Buttons(
    modifier: Modifier = Modifier,
    text: AnnotatedString,
    inlineContent: Map<String, InlineTextContent>,
) = Text(
    modifier = modifier,
    text = text,
    inlineContent = inlineContent,
    fontSize = INPUT_TEXT_SIZE,
    color = MaterialTheme.colors.onSurface
)

@Composable
internal fun Title(text: String, modifier: Modifier = Modifier) = Text(
    modifier = modifier,
    text = text,
    fontSize = 14.sp,
    fontStyle = FontStyle.Italic,
    color = MaterialTheme.colors.onSurface
)
