package andrew.misterio05.ui_mortalkombat.presentation.character.mappers

import andrew.misterio.design.createInlineImageRes
import andrew.misterio05.domain_mortalkombat.character.models.MoveType
import andrew.misterio05.ui_mortalkombat.R
import androidx.compose.foundation.text.InlineTextContent
import androidx.compose.ui.unit.em
import arrow.core.andThen

fun String.clearName() = replace('_', ' ')

val Map<Char, Int>.inlineContent: Map<String, InlineTextContent>
    get() = mapKeys(Map.Entry<Char, Int>::key andThen Char::toString)
        .mapValues { createInlineImageRes(it.value, 1.2.em) }

val MoveType.icon
    get() = when (this) {
        MoveType.OVERHEAD -> R.drawable.ic_overhead
        MoveType.HIGH -> R.drawable.ic_hight
        MoveType.MID -> R.drawable.ic_mid
        MoveType.LOW -> R.drawable.ic_low
        MoveType.UNKNOWN -> null
    }
