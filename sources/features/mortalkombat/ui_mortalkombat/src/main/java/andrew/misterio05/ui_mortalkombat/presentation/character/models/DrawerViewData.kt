package andrew.misterio05.ui_mortalkombat.presentation.character.models

import andrew.misterio05.domain_mortalkombat.character.models.Section
import andrew.misterio05.ui_mortalkombat.presentation.character.ui.drawer.SectionViewData
import android.net.Uri

data class DrawerViewData(
    val image: Uri,
    val name: String,
    val platforms: Set<PlatformViewItem>,
    val currentPlatform: Platform,
    val currentSection: Section,
    val sections: List<SectionViewData>,
)
