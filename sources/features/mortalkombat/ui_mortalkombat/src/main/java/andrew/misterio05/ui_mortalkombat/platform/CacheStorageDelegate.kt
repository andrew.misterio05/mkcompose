package andrew.misterio05.ui_mortalkombat.platform

import andrew.misterio05.data_mortalkombat.models.Data

class CacheStorageDelegate {

    var value: Cache<Data>? = null
        private set

    fun save(data: Data?) {
        value = data?.let { Cache(System.currentTimeMillis(), data) }
    }
}

data class Cache<T : Any>(val lastModify: Long, val data: T)
