package andrew.misterio05.ui_mortalkombat.presentation.character.models

import andrew.misterio.design.list.ListViewItem
import android.net.Uri

data class ScreenViewData(
    val id: String,
    val imageUri: Uri,
    val listState: ListState,
) {
    sealed class ListState {
        data class Data(val combos: List<ListViewItem>): ListState()
        object Loading: ListState()
    }
}
