package andrew.misterio05.ui_mortalkombat.presentation.character.ui.screen.buttons

import andrew.misterio.core.screenWidth
import andrew.misterio05.ui_mortalkombat.presentation.character.mappers.icon
import andrew.misterio05.ui_mortalkombat.presentation.character.models.ComboLineViewData
import andrew.misterio05.ui_mortalkombat.presentation.character.models.InlineTextViewData
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp

@Composable
internal fun Attack(
    moveDescription: ComboLineViewData.Description.Attack,
    value: InlineTextViewData,
) {
    var isShowingShortFrameData by remember { mutableStateOf(true) }
    Column(
        modifier = Modifier.clickable(
            interactionSource = remember { MutableInteractionSource() },
            indication = null,
            onClick = { isShowingShortFrameData = !isShowingShortFrameData }
        )
    ) {
        Row(verticalAlignment = Alignment.CenterVertically) {
            Buttons(text = value.text, inlineContent = value.keyToInlineInfo)
            Text(
                modifier = Modifier
                    .weight(1f)
                    .padding(4.dp),
                text = moveDescription.name,
                textAlign = TextAlign.End,
                color = MaterialTheme.colors.onSurface,
                maxLines = 2
            )
            AnimatedVisibility(
                visible = isShowingShortFrameData
            ) {
                ShortFrameDataComposable(
                    frameDataViewData = moveDescription.frameData
                )
            }
            moveDescription.type.icon?.let { icon ->
                Image(
                    modifier = Modifier.width(screenWidth / 9),
                    painter = painterResource(id = icon),
                    contentDescription = ""
                )
            }
                ?: Spacer(modifier = Modifier.width(8.dp))
        }
        AnimatedVisibility(visible = !isShowingShortFrameData) {
            OtherFrameDataComposable(
                frameDataViewData = moveDescription.frameData,
                modifier = Modifier.fillMaxWidth(),
                type = moveDescription.type,
            )
        }
    }
}
