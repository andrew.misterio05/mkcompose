package andrew.misterio05.ui_mortalkombat.presentation.character.ui

import andrew.misterio.core.CollectEvents
import andrew.misterio.core.koin.KoinScope
import andrew.misterio.core.koin.getViewModel
import andrew.misterio.core.state
import andrew.misterio05.ui_mortalkombat.CHARACTER_SCOPE
import andrew.misterio05.ui_mortalkombat.presentation.character.CharacterViewModel
import andrew.misterio05.ui_mortalkombat.presentation.character.Event
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import org.koin.core.parameter.parametersOf

@Composable
fun Mk11CharacterScreen(id: String) = KoinScope(CHARACTER_SCOPE) {
    val viewModel: CharacterViewModel = getViewModel { parametersOf(id) }
    val state by viewModel.state()
    val listState = rememberLazyListState()

    viewModel.CollectEvents { event ->
        when (event) {
            is Event.ScrollTop -> listState.animateScrollToItem(0)
        }
    }
    CharacterView(
        listState = listState,
        state = state,
        onTabSelected = viewModel::onTabSelected,
        onPlatformSelected = viewModel::onPlatformSelected,
    )
}
