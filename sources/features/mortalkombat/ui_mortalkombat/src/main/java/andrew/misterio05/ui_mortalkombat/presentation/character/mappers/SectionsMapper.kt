package andrew.misterio05.ui_mortalkombat.presentation.character.mappers

import andrew.misterio05.domain_mortalkombat.character.models.Section
import andrew.misterio05.ui_mortalkombat.presentation.character.ui.drawer.SectionViewData

class SectionsMapper {
    operator fun invoke(sections: Set<Section>): List<SectionViewData> = sections
        .map { SectionViewData(it, it.name.clearName()) }
}
