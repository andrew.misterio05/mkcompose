package andrew.misterio05.ui_mortalkombat.presentation.character.mappers

import andrew.misterio.design.list.ListViewItem
import andrew.misterio05.domain_mortalkombat.character.models.FrameData
import andrew.misterio05.domain_mortalkombat.character.models.MoveData
import andrew.misterio05.domain_mortalkombat.character.models.MoveDescription
import andrew.misterio05.ui_mortalkombat.presentation.character.models.ComboLineViewData
import andrew.misterio05.ui_mortalkombat.presentation.character.models.InlineTextViewData
import androidx.compose.foundation.text.InlineTextContent
import androidx.compose.foundation.text.appendInlineContent
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.buildAnnotatedString
import separatorViewData

class ComboItemMapper(
    private val buttonsStringPrepare: ButtonsStringPrepare
) {

    private val ComboLineViewData.Description.needSkipSeparator
        get() = when (this) {
            is ComboLineViewData.Description.Attack -> {
                extension == ComboLineViewData.Description.Attack.Extension.IS
            }
            else -> false
        }

    operator fun invoke(
        list: List<MoveData>,
        keys: Map<String, InlineTextContent>,
    ): List<ListViewItem> = list.flatMapIndexed { i, item ->
        val description = list.mapDescription(i, item.description)
        listOfNotNull(
            separatorViewData.takeIf { !description.needSkipSeparator },
            ComboLineViewData(
                value = InlineTextViewData(
                    text = buildAnnotatedString {
                        buttonsStringPrepare.execute(item.input).forEach {
                            val key = it.toString()
                            if (keys.containsKey(key)) {
                                appendInlineContent(key, "[Err]")
                            } else {
                                append(it)
                            }
                        }
                    },
                    keyToInlineInfo = keys
                ),
                description = description
            ),
            separatorViewData.takeIf { i == list.size - 1 },
        )
    }

    private fun List<MoveData>.mapDescription(
        index: Int,
        description: MoveDescription
    ): ComboLineViewData.Description = when (description) {
        is MoveDescription.Attack -> mapAttack(description, index)
        is MoveDescription.Combo -> mapCombo(description)
        is MoveDescription.Text -> mapSpecial(description)
    }

    private fun mapSpecial(description: MoveDescription.Text) =
        ComboLineViewData.Description.Special(
            name = description.name,
            description = description.value
        )

    private fun mapCombo(description: MoveDescription.Combo) = ComboLineViewData.Description.Kombo(
        leftPart = description.tinyDescription,
        rightPart = "DAMAGE: ${description.damage.toText()}"
    )

    private fun List<MoveData>.mapAttack(
        description: MoveDescription.Attack,
        index: Int
    ) = ComboLineViewData.Description.Attack(
        name = description.name,
        frameData = mapFrameData(description.frameData),
        type = description.type,
        extension = when {
            description.isExtra -> ComboLineViewData.Description.Attack.Extension.IS
            (getOrNull(index + 1)?.description as? MoveDescription.Attack)?.isExtra == true -> ComboLineViewData.Description.Attack.Extension.HAS
            else -> ComboLineViewData.Description.Attack.Extension.NOTHING
        }
    )

    private fun MoveDescription.Combo.Damage.toText(): String = when (this) {
        is MoveDescription.Combo.Damage.Range -> "${min.toInt()} - ${max.toInt()}"
        is MoveDescription.Combo.Damage.Exact -> value.toInt().toString()
    }

    private fun mapFrameData(frameData: FrameData) =
        ComboLineViewData.Description.Attack.FrameDataViewData(
            start = frameData.startUp.createColoredFrameDataText(
                gradient = arrayOf(10 to Color.Green)
            ),
            onBlock = frameData.blockAdvantage.createColoredFrameDataText(
                gradient = arrayOf(
                    -7 to Color.Red,
                    0 to Color(0xFFFF8000)
                )
            ),
            recovery = frameData.recovery.createColoredFrameDataText(),
            active = frameData.active.createColoredFrameDataText(),
            hit = frameData.hitAdvantage.createColoredFrameDataText(),
            cancel = frameData.cancelAdvantage.createColoredFrameDataText(),
            onFBlock = frameData.flawlessBlockAdvantage.createColoredFrameDataText(),
        )

    private fun Int.createColoredFrameDataText(
        name: String = "",
        vararg gradient: Pair<Int, Color>
    ) = ComboLineViewData.Description.Attack.FrameDataViewData.ColoredText(
        color = gradient.firstOrNull { this < it.first }?.second,
        value = "$name${":".takeIf { name.isNotEmpty() }.orEmpty()}${toString()}"
    )
}
