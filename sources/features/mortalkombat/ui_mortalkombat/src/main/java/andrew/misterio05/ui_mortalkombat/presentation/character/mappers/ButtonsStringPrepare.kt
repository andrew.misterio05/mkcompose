package andrew.misterio05.ui_mortalkombat.presentation.character.mappers

import PreparedButtons.AMPLIFY
import PreparedButtons.BLOCK
import PreparedButtons.CRUSHING
import PreparedButtons.INTERACT
import PreparedButtons.STANCE
import PreparedButtons.THROW

class ButtonsStringPrepare {
    private val extendedButtons = mapOf(
        "L1" to THROW,
        "L2" to STANCE,
        "R1" to INTERACT,
        "R2" to BLOCK,
        "/A" to AMPLIFY,
        "/C" to CRUSHING
    )
    fun execute(inputs: String): String {
        var result = inputs
        extendedButtons.forEach {
            if (result.contains(it.key)) {
                result = result.replace(it.key, it.value.toString())
            }
        }
        return result
    }
}
