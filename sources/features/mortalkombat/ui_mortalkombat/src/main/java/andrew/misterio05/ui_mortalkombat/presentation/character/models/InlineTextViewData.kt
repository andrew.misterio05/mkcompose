package andrew.misterio05.ui_mortalkombat.presentation.character.models

import androidx.compose.foundation.text.InlineTextContent
import androidx.compose.ui.text.AnnotatedString

data class InlineTextViewData(
    val text: AnnotatedString,
    val keyToInlineInfo: Map<String, InlineTextContent>
)
