package andrew.misterio05.ui_mortalkombat.presentation.character.ui.drawer

import andrew.misterio05.domain_mortalkombat.character.models.Section
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.ScrollableTabRow
import androidx.compose.material.Tab
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import arrow.core.andThen
import com.google.accompanist.pager.ExperimentalPagerApi

data class SectionViewData(
    val key: Section,
    val name: String
)

@Composable
fun SectionsPager(
    modifier: Modifier = Modifier,
    sections: List<SectionViewData>,
    current: Section,
    onSelected: (SectionViewData) -> Unit,
    onReSelected: (SectionViewData) -> Unit = {},
) = ScrollableTabRow(
    modifier = modifier,
    selectedTabIndex = sections
        .indexOfFirst(SectionViewData::key andThen current::equals)
        .coerceAtLeast(0),
    contentColor = MaterialTheme.colors.primary,
) {
    sections.forEach { data ->
        Tab(
            modifier = Modifier.padding(horizontal = 8.dp),
            selected = data.key == current,
            onClick = {
                when (data.key) {
                    current -> onReSelected
                    else -> onSelected
                }
                    .invoke(data)
            },
        ) {
            Text(
                text = data.name,
                color = MaterialTheme.colors.primary,
                fontSize = 24.sp,
            )
        }
    }
}
