package andrew.misterio05.ui_mortalkombat.presentation.character.ui.screen.buttons

import andrew.misterio05.ui_mortalkombat.presentation.character.models.ComboLineViewData
import andrew.misterio05.ui_mortalkombat.presentation.character.models.InlineTextViewData
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight

@Composable
internal fun Kombo(
    moveDescription: ComboLineViewData.Description.Kombo,
    value: InlineTextViewData,
) = Column {
    Buttons(value = value)
    Row {
        Title(modifier = Modifier.weight(1f), text = moveDescription.leftPart)
        Text(
            text = moveDescription.rightPart,
            fontWeight = FontWeight.ExtraBold,
            color = MaterialTheme.colors.primary
        )
    }
}
