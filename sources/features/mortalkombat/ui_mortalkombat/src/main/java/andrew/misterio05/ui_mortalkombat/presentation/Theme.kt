package andrew.misterio05.ui_mortalkombat.presentation

import androidx.compose.material.Colors
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

internal val colors: Colors
    @Composable get() = darkColors(
        primary = Color(0xFFf9bd3b),
        secondary = Color(0xFFf8eb78),
    )

@Composable
fun Mk11Theme(content: @Composable () -> Unit) = MaterialTheme(colors = colors, content = content)
