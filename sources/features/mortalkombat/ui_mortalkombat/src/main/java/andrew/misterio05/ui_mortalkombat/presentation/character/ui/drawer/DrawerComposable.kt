package andrew.misterio05.ui_mortalkombat.presentation.character.ui.drawer

import andrew.misterio.design.RES_NO_ID
import andrew.misterio.design.delegates.ResImage
import andrew.misterio05.domain_mortalkombat.character.models.Section
import andrew.misterio05.ui_mortalkombat.presentation.character.PADDINGS
import andrew.misterio05.ui_mortalkombat.presentation.character.models.DrawerViewData
import andrew.misterio05.ui_mortalkombat.presentation.character.models.Platform
import andrew.misterio05.ui_mortalkombat.presentation.character.models.PlatformViewItem
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import arrow.core.andThen
import coil.compose.AsyncImage

@Composable
fun DrawerContent(
    drawerViewData: DrawerViewData,
    onPlatformChanged: (Platform) -> Unit,
    onTabSelected: (Section) -> Unit,
) = Column(
    modifier = Modifier.fillMaxWidth(),
    horizontalAlignment = Alignment.CenterHorizontally
) {
    BottomTabs(
        modifier = Modifier
        .fillMaxWidth(),
        sections = drawerViewData.sections,
        current = drawerViewData.currentSection,
        onTabSelected = onTabSelected
    )
    val space = 16
    Spacer(modifier = Modifier.height(space.dp))
    AsyncImage(
        modifier = Modifier
            .fillMaxWidth(0.7f)
            .aspectRatio(1f),
        model = drawerViewData.image,
        contentDescription = null,
    )
    Spacer(modifier = Modifier.height(space.dp))
    Text(
        text = drawerViewData.name,
        fontSize = 36.sp,
        fontWeight = FontWeight.Black,
    )
    Spacer(modifier = Modifier.height(space.dp))
    Platforms(
        modifier = Modifier.fillMaxWidth(),
        current = drawerViewData.platforms
            .first(PlatformViewItem::id andThen drawerViewData.currentPlatform::equals)
            .id,
        items = drawerViewData.platforms,
        onSelected = onPlatformChanged,
    )
}

@Composable
private fun BottomTabs(
    modifier: Modifier,
    sections: List<SectionViewData>,
    current: Section,
    onTabSelected: (Section) -> Unit
) = Box(
    modifier = modifier.wrapContentHeight()
) {
    val onSelected = SectionViewData::key andThen onTabSelected
    SectionsPager(
        modifier = Modifier
            .wrapContentHeight()
            .padding(
                bottom = PADDINGS,
                top = PADDINGS
            ),
        sections = sections,
        current = current,
        onSelected = onSelected,
        onReSelected = onSelected,
    )
}

@Composable
private fun Platforms(
    modifier: Modifier = Modifier,
    items: Collection<PlatformViewItem>,
    current: Platform,
    onSelected: (Platform) -> Unit,
) = Row(
    modifier = modifier,
    horizontalArrangement = Arrangement.SpaceEvenly,
) {
    val spaceModifier = Modifier.weight(0.5f)
    items.forEach { item ->
        Spacer(modifier = spaceModifier)
        Box(
            modifier = Modifier
                .clickable { onSelected(item.id) }
                .selectableBackground(isSelected = current == item.id)
                .padding(8.dp)
                .weight(1f)
                .aspectRatio(1f)
        ) {
            val itemModifier = Modifier.align(Alignment.Center)
            when (item.iconResId) {
                RES_NO_ID -> {
                    Text(
                        modifier = itemModifier,
                        text = item.title,
                    )
                }
                else -> {
                    Image(
                        modifier = itemModifier.fillMaxSize(),
                        painter = painterResource(id = item.iconResId),
                        contentDescription = item.title
                    )
                }
            }
        }
    }
    Spacer(modifier = spaceModifier)
}

private fun Modifier.selectableBackground(isSelected: Boolean) = composed {
    val shape = RoundedCornerShape(corner = CornerSize(8.dp))
    when (isSelected) {
        true -> background(color = MaterialTheme.colors.primary, shape = shape)
        false -> border(color = MaterialTheme.colors.primary, width = 1.dp, shape = shape)
    }
}

@Composable
private fun PlatformItem(
    modifier: Modifier = Modifier,
    item: PlatformViewItem
) = Row(modifier) {
    ResImage(
        modifier = Modifier.height(IntrinsicSize.Max),
        id = item.iconResId
    )
    Text(text = item.title)
}
