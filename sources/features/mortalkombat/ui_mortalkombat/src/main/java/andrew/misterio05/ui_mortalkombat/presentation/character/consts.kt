package andrew.misterio05.ui_mortalkombat.presentation.character

import androidx.compose.ui.unit.dp

internal val PADDINGS = 4.dp
internal val STROKE_WIDTH = 1.dp
