package andrew.misterio05.ui_mortalkombat.presentation.character.mappers

import PlatformsButtons
import andrew.misterio05.ui_mortalkombat.presentation.character.models.Platform

class InlineTextContentMapper {

    operator fun invoke(platform: Platform) = when (platform) {
        Platform.PS -> PlatformsButtons.PS
        Platform.XBOX -> PlatformsButtons.XBOX
        Platform.NATIVE -> emptyMap()
    }
}
