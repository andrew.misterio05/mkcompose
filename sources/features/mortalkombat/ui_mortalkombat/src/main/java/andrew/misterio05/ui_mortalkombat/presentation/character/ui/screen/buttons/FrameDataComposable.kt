package andrew.misterio05.ui_mortalkombat.presentation.character.ui.screen.buttons

import andrew.misterio.design.list.Orientation
import andrew.misterio05.domain_mortalkombat.character.models.MoveType
import andrew.misterio05.ui_mortalkombat.presentation.character.models.ComboLineViewData
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.sp

@Composable
internal fun ShortFrameDataComposable(
    frameDataViewData: ComboLineViewData.Description.Attack.FrameDataViewData,
    modifier: Modifier = Modifier
) {
    @Composable
    fun FrameDataText(
        text: ComboLineViewData.Description.Attack.FrameDataViewData.ColoredText
    ) = Text(
        text = text.value,
        fontSize = 14.sp,
        color = text.color ?: MaterialTheme.colors.onSurface,
        fontWeight = FontWeight.ExtraBold,
        textAlign = TextAlign.Center
    )

    Column(
        modifier = modifier,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        FrameDataText(frameDataViewData.start)
        FrameDataText(frameDataViewData.onBlock)
    }
}

@Composable
internal fun OtherFrameDataComposable(
    frameDataViewData: ComboLineViewData.Description.Attack.FrameDataViewData,
    type: MoveType,
    modifier: Modifier = Modifier
) = Row(
    modifier = modifier,
    horizontalArrangement = Arrangement.Center,
    verticalAlignment = Alignment.CenterVertically
) {
    Column {
        FrameDataText("start ", frameDataViewData.start)
        FrameDataText("active ", frameDataViewData.active)
    }
    Spacer(modifier = Modifier.weight(1f))
    Column {
        FrameDataText("recovery ", frameDataViewData.recovery)
        FrameDataText("hit ", frameDataViewData.hit)
    }
    Spacer(modifier = Modifier.weight(1f))
    Column {
        FrameDataText("block ", frameDataViewData.onBlock)
        FrameDataText("f/block", frameDataViewData.onFBlock)
    }
    Spacer(modifier = Modifier.weight(1f))

    FrameDataText("cancel ", frameDataViewData.cancel)

    if (type != MoveType.UNKNOWN) {
        Spacer(modifier = Modifier.weight(1f))
        Text(
            text = type.name,
            color = MaterialTheme.colors.onSurface,
        )
    }
    // Spacer(modifier = Modifier.weight(1f))
}

@Composable
private fun FrameDataText(
    title: String,
    text: ComboLineViewData.Description.Attack.FrameDataViewData.ColoredText,
    orientation: Orientation = Orientation.Horizontal(
        alignment = Alignment.CenterVertically
    )
) {
    @Composable
    fun DrawFrame(
        title: String,
        text: ComboLineViewData.Description.Attack.FrameDataViewData.ColoredText
    ) {
        Text(
            modifier = Modifier.alpha(0.5f),
            text = title.uppercase(),
            fontSize = 12.sp,
            color = MaterialTheme.colors.onSurface,
            fontWeight = FontWeight.Bold,
            textAlign = TextAlign.Center,
        )
        Text(
            text = text.value,
            fontSize = 14.sp,
            color = text.color ?: MaterialTheme.colors.onSurface,
            fontWeight = FontWeight.Bold,
            textAlign = TextAlign.Center
        )
    }
    when (orientation) {
        is Orientation.Horizontal -> Row(
            horizontalArrangement = orientation.arrangement,
            verticalAlignment = orientation.alignment,
            content = { DrawFrame(title = title, text = text) }
        )
        is Orientation.Vertical -> Column(
            verticalArrangement = orientation.arrangement,
            horizontalAlignment = orientation.alignment,
            content = { DrawFrame(title = title, text = text) }
        )
    }
}
