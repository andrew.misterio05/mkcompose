package andrew.misterio05.ui_mortalkombat.presentation.character.ui.screen

import androidx.compose.ui.unit.sp

internal val INPUT_TEXT_SIZE = 28.sp
