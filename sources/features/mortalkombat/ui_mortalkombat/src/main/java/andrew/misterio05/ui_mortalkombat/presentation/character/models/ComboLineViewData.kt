package andrew.misterio05.ui_mortalkombat.presentation.character.models

import andrew.misterio.design.list.ListViewItem
import andrew.misterio05.domain_mortalkombat.character.models.MoveType
import androidx.compose.ui.graphics.Color

data class ComboLineViewData(
    val value: InlineTextViewData,
    val description: Description
) : ListViewItem {
    override val key: ListViewItem.Key = ComboLineViewData
    sealed class Description {
        data class Kombo(
            val leftPart: String,
            val rightPart: String
        ) : Description()

        data class Special(
            val name: String,
            val description: String
        ) : Description()

        data class Attack(
            val name: String,
            val frameData: FrameDataViewData,
            val type: MoveType,
            val extension: Extension
        ) : Description() {
            enum class Extension { HAS, IS, NOTHING }
            data class FrameDataViewData(
                val start: ColoredText,
                val active: ColoredText,
                val recovery: ColoredText,
                val cancel: ColoredText,
                val hit: ColoredText,
                val onBlock: ColoredText,
                val onFBlock: ColoredText,
            ) {
                data class ColoredText(val color: Color?, val value: String)
            }
        }
    }

    companion object : ListViewItem.Key
}
