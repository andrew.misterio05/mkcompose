package andrew.misterio05.ui_mortalkombat.presentation.character

import PreparedButtons
import andrew.misterio.core.BaseViewModel
import andrew.misterio.design.RES_NO_ID
import andrew.misterio05.data_mortalkombat.createImagePath
import andrew.misterio05.domain_mortalkombat.character.GetCharacterInteractor
import andrew.misterio05.domain_mortalkombat.character.models.Section
import andrew.misterio05.ui_mortalkombat.presentation.character.mappers.ComboItemMapper
import andrew.misterio05.ui_mortalkombat.presentation.character.mappers.InlineTextContentMapper
import andrew.misterio05.ui_mortalkombat.presentation.character.mappers.SectionsMapper
import andrew.misterio05.ui_mortalkombat.presentation.character.mappers.clearName
import andrew.misterio05.ui_mortalkombat.presentation.character.mappers.inlineContent
import andrew.misterio05.ui_mortalkombat.presentation.character.models.DrawerViewData
import andrew.misterio05.ui_mortalkombat.presentation.character.models.Platform
import andrew.misterio05.ui_mortalkombat.presentation.character.models.PlatformViewItem
import andrew.misterio05.ui_mortalkombat.presentation.character.models.ScreenViewData
import android.net.Uri
import org.orbitmvi.orbit.syntax.simple.reduce

data class StateData(
    val drawer: DrawerViewData,
    val screen: ScreenViewData,
)

sealed class Event {
    object ScrollTop : Event()
}

private val DEFAULT_TAB = Section.values().first()
private val DEFAULT_PLATFORM = Platform.values().first()

private fun createInitSate(
    characterId: String,
    sectionsMapper: SectionsMapper,
    platforms: Set<PlatformViewItem>,
): StateData {
    val image = Uri.parse("file:///android_asset/${createImagePath(characterId)}")
    return StateData(
        screen = ScreenViewData(
            id = characterId,
            imageUri = image,
            listState = ScreenViewData.ListState.Loading,
        ),
        drawer = DrawerViewData(
            image = image,
            name = characterId,
            platforms = platforms,
            currentSection = DEFAULT_TAB,
            sections = sectionsMapper(Section.values().toSet()),
            currentPlatform = DEFAULT_PLATFORM,
        )
    )
}

class CharacterViewModel(
    private val characterId: String,
    private val getCharacterInteractor: GetCharacterInteractor,
    private val inlineTextContentMapper: InlineTextContentMapper,
    private val comboItemMapper: ComboItemMapper,
    sectionsMapper: SectionsMapper,
) : BaseViewModel<StateData, Event>(
    createInitSate(
        characterId = characterId,
        sectionsMapper = sectionsMapper,
        platforms = inlineTextContentMapper.mapPlatforms(Platform.values().toList())
    )
) {

    init {
        loadData()
    }

    fun onTabSelected(tab: Section) = loadData(tab = tab)

    fun onPlatformSelected(platform: Platform) = loadData(platform = platform)

    private fun loadData(
        tab: Section = state.value.drawer.currentSection,
        platform: Platform = state.value.drawer.currentPlatform,
    ) = intent {
        val list = getCharacterInteractor.execute(characterId).let { character ->
            comboItemMapper(
                list = character.data[tab].orEmpty(),
                keys = inlineTextContentMapper(platform).inlineContent,
            )
        }
        reduce {
            state.copy(
                screen = state.screen.copy(
                    listState = ScreenViewData.ListState.Data(list),
                ),
                drawer = state.drawer.copy(
                    currentSection = tab,
                    currentPlatform = platform
                ),
            )
        }
    }
}

private fun InlineTextContentMapper.mapPlatforms(platforms: Collection<Platform>) = platforms
    .map { platform ->
        PlatformViewItem(
            id = platform,
            iconResId = invoke(platform)[PreparedButtons.FK] ?: RES_NO_ID,
            title = platform.name.clearName(),
        )
    }
    .toSet()
