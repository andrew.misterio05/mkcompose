package andrew.misterio05.ui_mortalkombat.presentation.characters

import andrew.misterio05.domain_mortalkombat.characters.CharactersRemoteRepo

class Mk11CharactersListRepo internal constructor(
    private val charactersRemoteRepo: CharactersRemoteRepo
) {
    suspend operator fun <T> invoke(map: (String, String, String) -> T): List<T> =
        charactersRemoteRepo.getList()
            .map { map(it.id, it.imageUrl, it.name) }
}
