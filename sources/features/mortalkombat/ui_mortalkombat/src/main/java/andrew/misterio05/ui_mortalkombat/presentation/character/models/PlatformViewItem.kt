package andrew.misterio05.ui_mortalkombat.presentation.character.models

import andrew.misterio.design.RES_NO_ID
import androidx.annotation.DrawableRes

data class PlatformViewItem(
    val id: Platform,
    @DrawableRes
    val iconResId: Int = RES_NO_ID,
    val title: String
) {
    override fun equals(other: Any?): Boolean = when (other) {
        is PlatformViewItem -> other.id == id
        else -> super.equals(other)
    }

    override fun hashCode(): Int = id.hashCode()
}
