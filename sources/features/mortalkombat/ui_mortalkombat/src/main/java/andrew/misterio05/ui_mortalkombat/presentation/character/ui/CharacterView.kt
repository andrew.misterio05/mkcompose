package andrew.misterio05.ui_mortalkombat.presentation.character.ui

import andrew.misterio05.domain_mortalkombat.character.models.Section
import andrew.misterio05.ui_mortalkombat.presentation.Mk11Theme
import andrew.misterio05.ui_mortalkombat.presentation.character.StateData
import andrew.misterio05.ui_mortalkombat.presentation.character.models.Platform
import andrew.misterio05.ui_mortalkombat.presentation.character.ui.drawer.DrawerContent
import andrew.misterio05.ui_mortalkombat.presentation.character.ui.screen.Content
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.material.BottomSheetScaffold
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun CharacterView(
    listState: LazyListState,
    state: StateData,
    onTabSelected: (Section) -> Unit,
    onPlatformSelected: (Platform) -> Unit,
) = Mk11Theme {
    BottomSheetScaffold(
        modifier = Modifier
            .background(MaterialTheme.colors.background)
            .fillMaxSize(),
        sheetPeekHeight = 36.dp,
        sheetContent = {
            DrawerContent(
                drawerViewData = state.drawer,
                onPlatformChanged = onPlatformSelected,
                onTabSelected = onTabSelected,
            )
        },
    ) {
        Content(
            modifier = Modifier.padding(it),
            listState = listState,
            screenViewData = state.screen,
        )
    }
}
