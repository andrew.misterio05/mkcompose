package andrew.misterio05.ui_mortalkombat

import andrew.misterio05.data_mortalkombat.CharacterRemoteRepoImpl
import andrew.misterio05.data_mortalkombat.CharactersRemoteRepoImpl
import andrew.misterio05.data_mortalkombat.DataStorage
import andrew.misterio05.domain_mortalkombat.character.CharacterRemoteRepo
import andrew.misterio05.domain_mortalkombat.character.GetCharacterInteractor
import andrew.misterio05.domain_mortalkombat.characters.CharactersRemoteRepo
import andrew.misterio05.ui_mortalkombat.platform.CacheStorageDelegate
import andrew.misterio05.ui_mortalkombat.platform.DataStorageImpl
import andrew.misterio05.ui_mortalkombat.platform.LocaleFileStorageDelegate
import andrew.misterio05.ui_mortalkombat.presentation.character.CharacterViewModel
import andrew.misterio05.ui_mortalkombat.presentation.character.mappers.ButtonsStringPrepare
import andrew.misterio05.ui_mortalkombat.presentation.character.mappers.ComboItemMapper
import andrew.misterio05.ui_mortalkombat.presentation.character.mappers.InlineTextContentMapper
import andrew.misterio05.ui_mortalkombat.presentation.character.mappers.SectionsMapper
import andrew.misterio05.ui_mortalkombat.presentation.characters.Mk11CharactersListRepo
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.dsl.factoryOf
import org.koin.core.module.dsl.scopedOf
import org.koin.core.module.dsl.singleOf
import org.koin.core.qualifier.named
import org.koin.dsl.ScopeDSL
import org.koin.dsl.module

internal const val CHARACTER_SCOPE = "scope of character detail screen"

val diMortalKombat11 = module {
    singleOf(::LocaleFileStorageDelegate)
    singleOf(::CacheStorageDelegate)
    single<DataStorage> { DataStorageImpl(get(), get()) }
    factory<CharactersRemoteRepo> { CharactersRemoteRepoImpl(get()) }
    factoryOf(::Mk11CharactersListRepo)
    scope(named(CHARACTER_SCOPE), ::character)
}

private fun character(dsl: ScopeDSL) = with(dsl) {
    scopedOf(::SectionsMapper)
    scopedOf(::ComboItemMapper)
    scopedOf(::ButtonsStringPrepare)
    scopedOf(::GetCharacterInteractor)
    scopedOf(::InlineTextContentMapper)
    factory<CharacterRemoteRepo> { CharacterRemoteRepoImpl(get()) }
    viewModel { (id: String) -> CharacterViewModel(id, get(), get(), get(), get()) }
}
