package andrew.misterio05.ui_mortalkombat.presentation.character.models

enum class Platform {
    PS, XBOX, NATIVE;
}
