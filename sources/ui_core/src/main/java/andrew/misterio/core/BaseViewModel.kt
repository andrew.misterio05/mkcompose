package andrew.misterio.core

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow
import org.orbitmvi.orbit.Container
import org.orbitmvi.orbit.ContainerHost
import org.orbitmvi.orbit.syntax.OrbitDsl
import org.orbitmvi.orbit.syntax.simple.SimpleSyntax
import org.orbitmvi.orbit.syntax.simple.intent
import org.orbitmvi.orbit.viewmodel.container

open class BaseViewModel<STATE : Any, EVENT : Any>(initSate: STATE) : ViewModel() {

    private val orbit = ContainerHolder(initSate = initSate)
    val state: StateFlow<STATE> = orbit.container.stateFlow
    val sideEffect: Flow<EVENT> = orbit.container.sideEffectFlow

    @OrbitDsl
    protected fun intent(
        registerIdling: Boolean = true,
        transformer: suspend SimpleSyntax<STATE, EVENT>.() -> Unit
    ): Unit = orbit.intent(registerIdling, transformer)

    private inner class ContainerHolder(initSate: STATE) : ContainerHost<STATE, EVENT> {
        override val container: Container<STATE, EVENT> = this@BaseViewModel.container(initSate)
    }
}
