package andrew.misterio.core.koin

import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.remember
import org.koin.core.Koin
import org.koin.core.annotation.KoinInternalApi
import org.koin.core.context.GlobalContext
import org.koin.core.qualifier.StringQualifier
import org.koin.core.scope.Scope
import org.koin.ext.getFullName
import org.koin.mp.KoinPlatformTools

@OptIn(KoinInternalApi::class)
@PublishedApi
internal val LocalScope = compositionLocalOf {
    GlobalContext.get().scopeRegistry.rootScope
}

@Composable
inline fun <reified T : Any> KoinScope(
    noinline content: @Composable () -> Unit,
) = KoinScope(
    name = T::class.getFullName(),
    content = content,
)

@Composable
fun KoinScope(
    name: String,
    content: @Composable () -> Unit,
) = KoinScope(
    getScope = {
        createScope(
            scopeId = KoinPlatformTools.generateId(),
            qualifier = StringQualifier(name),
        )
    },
    content = content,
)

@Composable
fun KoinScope(
    getScope: Koin.() -> Scope,
    content: @Composable () -> Unit,
) {
    val currentScope = LocalScope.current
    val koin = GlobalContext.get()
    val scope = remember {
        koin.getScope().also {
            it.linkTo(currentScope)
        }
    }
    CompositionLocalProvider(LocalScope provides scope) {
        content()
        DisposableEffect(key1 = scope) {
            onDispose(scope::close)
        }
    }
}
