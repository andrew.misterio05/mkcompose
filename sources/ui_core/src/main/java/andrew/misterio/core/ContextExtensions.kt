package andrew.misterio.core

import android.content.Context
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.TextUnit

val Context.screenWidth: Int get() = resources.displayMetrics.widthPixels
val Context.screenHeight: Int get() = resources.displayMetrics.heightPixels

val screenWidth: Dp @Composable get() = LocalDensity.current.run {
    LocalContext.current.screenWidth.toDp()
}
val screenHeight: Dp @Composable get() = LocalDensity.current.run {
    LocalContext.current.screenHeight.toDp()
}

@Composable
fun Int.toDp(): Dp = LocalDensity.current.run { this@toDp.toDp() }

@Composable
fun Dp.toSp(): TextUnit = LocalDensity.current.run { this@toSp.toSp() }
