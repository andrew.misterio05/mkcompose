package andrew.misterio.core.koin

import org.koin.core.module.Module
import org.koin.core.qualifier.named
import org.koin.dsl.ScopeDSL
import org.koin.dsl.module

fun di(
    name: String,
    binds: ScopeDSL.() -> Unit,
): Module = module {
    scope(named(name), binds)
}
