package andrew.misterio.core

import androidx.compose.runtime.Composable
import androidx.navigation.NoOpNavigator

@Composable
fun rememberNavController() = androidx.navigation.compose.rememberNavController(NoOpNavigator())
