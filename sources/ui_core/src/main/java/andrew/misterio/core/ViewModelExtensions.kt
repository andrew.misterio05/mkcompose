package andrew.misterio.core

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.State
import androidx.compose.runtime.collectAsState
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@Composable
fun <S : Any, E : Any> BaseViewModel<S, E>.state(): State<S> = state.collectAsState()

@Composable
fun <T : Any> BaseViewModel<*, T>.CollectEvents(action: suspend (T) -> Unit) {
    LaunchedEffect(key1 = this) {
        launch {
            sideEffect.collect(action = action)
        }
    }
}
