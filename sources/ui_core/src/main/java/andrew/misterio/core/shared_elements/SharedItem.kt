package andrew.misterio.core.shared_elements

import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.unit.IntSize

sealed class SharedItem {
    data class Params(
        val position: Offset,
        val size: IntSize,
    ) : SharedItem()

    object Unknown : SharedItem()
}
