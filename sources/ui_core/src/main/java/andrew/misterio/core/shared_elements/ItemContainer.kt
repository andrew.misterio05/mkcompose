package andrew.misterio.core.shared_elements

import androidx.compose.runtime.compositionLocalOf

data class ItemContainer(
    private val container: MutableMap<String, SharedItem> = mutableMapOf()
) {
    operator fun get(key: String): SharedItem = container.getOrElse(key) { SharedItem.Unknown }

    operator fun set(key: String, value: SharedItem) {
        container[key] = value
    }
}

val LocalSharedContainer = compositionLocalOf {
    ItemContainer()
}
