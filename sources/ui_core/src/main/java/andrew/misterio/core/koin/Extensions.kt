package andrew.misterio.core.koin

import androidx.compose.runtime.Composable
import androidx.lifecycle.ViewModel
import org.koin.core.parameter.ParametersDefinition
import org.koin.core.qualifier.Qualifier

@Composable
inline fun <reified T : ViewModel> getViewModel(
    qualifier: Qualifier? = null,
    noinline parameters: ParametersDefinition? = null,
): T = org.koin.androidx.compose.getViewModel(
    qualifier = qualifier,
    scope = LocalScope.current,
    parameters = parameters,
)
