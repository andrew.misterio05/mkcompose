package andrew.misterio.design.list

fun List<ListViewItem>.insertSeparator(
    separatorViewItem: SeparatorViewItem,
    isEdgesIncluded: Boolean = false
): List<ListViewItem> = flatMap { item ->
    listOfNotNull(
        separatorViewItem.takeIf { isEdgesIncluded && item === first() },
        item,
        separatorViewItem.takeIf { item !== last() || item === last() && isEdgesIncluded }
    )
}
