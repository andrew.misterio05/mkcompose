package andrew.misterio.design

import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.ui.Modifier

fun Modifier.square() = aspectRatio(1f)
