package andrew.misterio.design

import androidx.compose.material.DropdownMenu
import androidx.compose.material.DropdownMenuItem
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier

@Composable
fun <T : Any> Spinner(
    modifier: Modifier = Modifier,
    expandedStates: MutableState<Boolean> = remember { mutableStateOf(false) },
    items: Iterable<T>,
    selectedItem: T = items.first(),
    onClick: (T) -> Unit = {},
    content: @Composable (T) -> Unit
) {
    var isExpanded by expandedStates
    val dismiss = { isExpanded = false }

    DropdownMenu(
        expanded = isExpanded,
        onDismissRequest = dismiss,
    ) {
        items.forEach {
            DropDownItem(
                modifier = modifier,
                item = it,
                onClick = {
                    onClick(it)
                    dismiss()
                },
                content = content
            )
        }
    }
    DropDownItem(
        modifier = modifier,
        item = selectedItem,
        onClick = { isExpanded = true },
        content = content
    )
}

@Composable
private fun <T : Any> DropDownItem(
    item: T,
    modifier: Modifier = Modifier,
    onClick: () -> Unit,
    content: @Composable (T) -> Unit
) = DropdownMenuItem(
    modifier = modifier,
    onClick = { onClick() },
    content = { content(item) }
)
