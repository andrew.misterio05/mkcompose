package andrew.misterio.design

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.graphics.painter.BitmapPainter
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.platform.LocalContext
import arrow.core.andThen
import java.io.InputStream

private val InputStream.toBitmap get() = BitmapFactory.decodeStream(this)

@Composable
fun painterAssets(path: String): Painter = BitmapPainter(
    image = LocalContext.current.assets.open(path)
        .let(InputStream::toBitmap andThen Bitmap::asImageBitmap)
)
