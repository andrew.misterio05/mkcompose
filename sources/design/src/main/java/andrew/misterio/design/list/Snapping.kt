package andrew.misterio.design.list

import androidx.compose.animation.core.Animatable
import androidx.compose.foundation.gestures.FlingBehavior
import androidx.compose.foundation.gestures.ScrollScope
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember

@Composable
fun rememberSnappingFlingBehavior(state: LazyListState) = remember {
    object : FlingBehavior {
        override suspend fun ScrollScope.performFling(initialVelocity: Float): Float {
            val firstItem = state.layoutInfo.visibleItemsInfo.firstOrNull() ?: return 0f
            val visibilityRate = -firstItem.offset.toFloat() / firstItem.size
            val scrollBy = if (visibilityRate > 0.5f) {
                firstItem.run { size + offset }
            } else {
                firstItem.offset
            }
            var lastVal = 0f
            Animatable(0f)
                .animateTo(
                    targetValue = scrollBy.toFloat(),
                    initialVelocity = initialVelocity
                ) {
                    val newDelta = value - lastVal
                    scrollBy(newDelta)
                    lastVal = value
                }
            return 0f
        }
    }
}
