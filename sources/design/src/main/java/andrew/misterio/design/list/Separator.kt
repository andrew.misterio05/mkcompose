package andrew.misterio.design.list

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.width
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.Dp

fun separatorVerticalDelegate() = listDelegate<SeparatorViewItem>(key = SeparatorViewItem) { data ->
    Spacer(
        Modifier
            .fillParentMaxWidth()
            .height(data.thickness)
            .background(color = data.color)
    )
}

fun separatorHorizontalDelegate() = listDelegate<SeparatorViewItem>(key = SeparatorViewItem) { data ->
    Spacer(
        Modifier
            .heightIn()
            .width(data.thickness)
            .background(color = data.color)
    )
}

data class SeparatorViewItem(
    val color: Color = Color.Transparent,
    val thickness: Dp
) : ListViewItem {
    override val key get() = SeparatorViewItem
    companion object : ListViewItem.Key
}
