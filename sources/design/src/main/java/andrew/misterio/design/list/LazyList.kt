package andrew.misterio.design.list

import androidx.compose.foundation.gestures.FlingBehavior
import androidx.compose.foundation.gestures.ScrollableDefaults
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyItemScope
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier

sealed class Orientation {
    data class Vertical(
        val alignment: Alignment.Horizontal = Alignment.Start,
        val arrangement: Arrangement.Vertical = Arrangement.Top
    ) : Orientation()

    data class Horizontal(
        val alignment: Alignment.Vertical = Alignment.Top,
        val arrangement: Arrangement.Horizontal = Arrangement.Start
    ) : Orientation()
}

interface ListViewItem {
    val key: Key
    interface Key
}

@Composable
fun <T : ListViewItem> LazyList(
    items: List<T>,
    modifier: Modifier = Modifier,
    flingBehavior: FlingBehavior = ScrollableDefaults.flingBehavior(),
    state: LazyListState = rememberLazyListState(),
    orientation: Orientation = Orientation.Vertical(),
    itemContent: @Composable LazyItemScope.(T) -> Unit
) {
    val content: LazyListScope.() -> Unit = { items(items) { itemContent(it) } }

    return when (orientation) {
        is Orientation.Vertical -> LazyColumn(
            modifier = modifier,
            flingBehavior = flingBehavior,
            horizontalAlignment = orientation.alignment,
            verticalArrangement = orientation.arrangement,
            content = content,
            state = state
        )
        is Orientation.Horizontal -> LazyRow(
            modifier = modifier,
            flingBehavior = flingBehavior,
            verticalAlignment = orientation.alignment,
            horizontalArrangement = orientation.arrangement,
            content = content,
            state = state
        )
    }
}
