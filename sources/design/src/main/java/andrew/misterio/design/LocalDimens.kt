package andrew.misterio.design

import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.Stable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.runtime.staticCompositionLocalOf
import androidx.compose.runtime.structuralEqualityPolicy
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

val LocalDimens = staticCompositionLocalOf { standardDimens() }

val MaterialTheme.dimens: Dimens @Composable get() = LocalDimens.current

fun standardDimens(
    paddings: Sizes = Sizes(
        small = 8.dp,
        medium = 16.dp,
        large = 24.dp,
    ),
    elevation: Sizes = Sizes(
        small = 8.dp,
        medium = 16.dp,
        large = 24.dp,
    ),
) = Dimens(
    paddings = paddings,
    elevation = elevation,
)

@Stable
class Dimens(
    paddings: Sizes,
    elevation: Sizes,
) {
    var paddings by mutableStateOf(paddings, structuralEqualityPolicy())
        internal set
    var elevation by mutableStateOf(elevation, structuralEqualityPolicy())
        internal set

    fun copy(
        paddings: Sizes,
        elevation: Sizes,
    ) = Dimens(
        paddings = this.paddings,
        elevation = this.elevation,
    )

    override fun toString(): String = "Dimens(" +
        "paddings=${this.paddings}" +
        "elevation=${this.elevation}" +
        ")"
}

data class Sizes(
    val small: Dp,
    val medium: Dp,
    val large: Dp,
)
