package andrew.misterio.design

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.InlineTextContent
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.Placeholder
import androidx.compose.ui.text.PlaceholderVerticalAlign
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.em

fun createInlineImageRes(imageId: Int, size: TextUnit = 1.em) =
    createInlineImageRes(imageId, size, size)

fun createInlineImageRes(
    imageId: Int,
    width: TextUnit = 1.em,
    height: TextUnit = 1.em
): InlineTextContent = InlineTextContent(
    placeholder = Placeholder(
        width = width,
        height = height,
        placeholderVerticalAlign = PlaceholderVerticalAlign.TextCenter
    ),
    children = {
        Image(
            painter = painterResource(id = imageId),
            contentDescription = "",
            modifier = Modifier.fillMaxSize(),
            contentScale = ContentScale.FillBounds,
        )
    }
)

@Composable
fun SimpleTextMessage(text: String) = Text(
    text = text,
    modifier = Modifier.padding(MaterialTheme.dimens.paddings.small),
    color = MaterialTheme.colors.onBackground
)
