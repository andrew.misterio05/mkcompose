package andrew.misterio.design.delegates

import andrew.misterio.design.RES_NO_ID
import androidx.annotation.DrawableRes
import androidx.compose.foundation.Image
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource

@Composable
fun ResImage(
    modifier: Modifier = Modifier,
    @DrawableRes id: Int,
    contentDescription: String = ""
) {
    if (id != RES_NO_ID) {
        Image(
            modifier = modifier,
            painter = painterResource(id = id),
            contentDescription = contentDescription
        )
    }
}
