package andrew.misterio.design.list

import androidx.compose.foundation.gestures.FlingBehavior
import androidx.compose.foundation.gestures.ScrollableDefaults
import androidx.compose.foundation.lazy.LazyItemScope
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier

typealias ListDelegateBinder = @Composable LazyItemScope.(ListViewItem) -> Unit
typealias ListDelegate = Pair<ListViewItem.Key, ListDelegateBinder>

@Composable
fun DelegatedLazyList(
    items: List<ListViewItem>,
    modifier: Modifier = Modifier,
    state: LazyListState = rememberLazyListState(),
    flingBehavior: FlingBehavior = ScrollableDefaults.flingBehavior(),
    delegates: Map<ListViewItem.Key, ListDelegateBinder>,
    orientation: Orientation = Orientation.Vertical(Alignment.Start),
) = LazyList(
    items = items,
    modifier = modifier,
    state = state,
    flingBehavior = flingBehavior,
    orientation = orientation,
) { item -> delegates[item.key]?.invoke(this, item) }

fun <T : ListViewItem> listDelegate(
    key: ListViewItem.Key,
    content: @Composable LazyItemScope.(T) -> Unit
): ListDelegate = key to { content(it as T) }
