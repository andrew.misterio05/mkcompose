package andrew.misterio.design.list

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

typealias VerticalGridDelegate = @Composable RowScope.(ListViewItem) -> Unit

@Composable
fun <T : ListViewItem> DelegatedLazyVerticalGrid(
    items: List<T>,
    modifier: Modifier = Modifier,
    rowSize: Int,
    vararg delegates: VerticalGridDelegate,
    horizontalArrangement: Arrangement.Horizontal = Arrangement.Start,
    verticalArrangement: Arrangement.Vertical = Arrangement.Top
) {
    val rows = items.chunked(rowSize)
    LazyColumn(
        modifier,
        verticalArrangement = verticalArrangement
    ) {
        items(rows) { row ->
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentHeight(),
                horizontalArrangement = horizontalArrangement
            ) {
                row.forEach {
                    delegates.forEach { delegate -> delegate(it) }
                }
            }
        }
    }
}

@Composable
inline fun <reified T : ListViewItem> verticalGridDelegate(
    crossinline content: @Composable RowScope.(T) -> Unit
): VerticalGridDelegate = { if (it is T) content(it) }
