package andrew.misterio.mkcompose.ui.navigation

sealed class GraphNode {
    open val route: String get() = this::class.run { simpleName ?: java.name }

    abstract class OneArgNode(val argKey: String) : GraphNode() {
        override val route: String = createRoute(value = "{$argKey}")
        fun createRoute(value: String): String = super.route + "/$value"
    }

    object Home : GraphNode()
    object Characters : OneArgNode("game")
    object Mk11Character : OneArgNode("id")
}
