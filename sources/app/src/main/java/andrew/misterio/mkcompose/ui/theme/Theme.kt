package andrew.misterio.mkcompose.ui.theme

import andrew.misterio.design.LocalDimens
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider

@Composable
fun GeneralTheme(
    isDarkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable () -> Unit
) = MaterialTheme(
    colors = when {
        isDarkTheme -> darkColorPalette()
        else -> lightColorPalette()
    },
    typography = Typography,
    shapes = Shapes,
) {
    CompositionLocalProvider(
        values = arrayOf(
            LocalDimens provides dimens,
        ),
        content = content,
    )
}
