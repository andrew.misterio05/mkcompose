package andrew.misterio.mkcompose.di

import andrew.misterio.mkcompose.ui.MainActivity
import andrew.misterio.mkcompose.ui.navigation.AppReadyStateHolder
import andrew.misterio.mkcompose.ui.navigation.GraphProvider
import org.koin.core.module.dsl.scopedOf
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.module

val diMain = module {
    singleOf(::AppReadyStateHolder)
    scope<MainActivity> {
        scopedOf(::GraphProvider)
    }
}
