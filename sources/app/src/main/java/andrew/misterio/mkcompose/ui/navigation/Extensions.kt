package andrew.misterio.mkcompose.ui.navigation

import androidx.navigation.NavController

operator fun NavController.invoke(actions: NavController.() -> Unit): () -> Unit = {
    actions()
}

operator fun <T> NavController.invoke(actions: NavController.(T) -> Unit): (T) -> Unit = {
    actions(it)
}
