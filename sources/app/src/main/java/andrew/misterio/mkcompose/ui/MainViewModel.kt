package andrew.misterio.mkcompose.ui

import andrew.misterio.core.BaseViewModel

object State

sealed class Event {
    object Ready : Event()
}

class MainViewModel() : BaseViewModel<State, Event>(State) {

    init {
    }
}
