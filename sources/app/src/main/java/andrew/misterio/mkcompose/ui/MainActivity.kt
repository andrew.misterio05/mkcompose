package andrew.misterio.mkcompose.ui

import andrew.misterio.core.shared_elements.ItemContainer
import andrew.misterio.core.shared_elements.LocalSharedContainer
import andrew.misterio.mkcompose.ui.navigation.AppReadyStateHolder
import andrew.misterio.mkcompose.ui.navigation.GraphNode
import andrew.misterio.mkcompose.ui.navigation.GraphProvider
import andrew.misterio.mkcompose.ui.theme.GeneralTheme
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.rememberNavController
import arrow.core.andThen
import arrow.core.partially2
import org.koin.android.ext.android.inject
import org.koin.android.scope.AndroidScopeComponent
import org.koin.androidx.scope.activityRetainedScope
import org.koin.core.scope.Scope

class MainActivity : AppCompatActivity(), AndroidScopeComponent {

    override val scope: Scope by activityRetainedScope()
    private val graphProvider: GraphProvider by inject()
    private val isSplashScreenKeepWrapper: AppReadyStateHolder by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        val splashScreen = installSplashScreen()
        super.onCreate(savedInstanceState)
        splashScreen.setKeepOnScreenCondition(isSplashScreenKeepWrapper::state andThen Boolean::not)
        content {
            val navController = rememberNavController()
            NavHost(
                navController = navController,
                startDestination = GraphNode.Home.route,
                builder = graphProvider::provide.partially2(p2 = navController)
            )
        }
    }

    private fun content(content: @Composable () -> Unit) = setContent {
        GeneralTheme {
            CompositionLocalProvider(
                LocalSharedContainer provides ItemContainer(),
            ) {
                content()
            }
        }
    }
}
