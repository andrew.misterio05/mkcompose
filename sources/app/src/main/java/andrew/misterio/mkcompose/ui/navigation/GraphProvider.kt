package andrew.misterio.mkcompose.ui.navigation

import andrew.misterio.domain_characters.Character
import andrew.misterio.ui_characters.presentation.view.CharactersScreen
import andrew.misterio.ui_home.HomeRouter
import andrew.misterio.ui_home.HomeScreen
import andrew.misterio05.ui_mortalkombat.presentation.Mk11Theme
import andrew.misterio05.ui_mortalkombat.presentation.character.ui.Mk11CharacterScreen
import andrew.misterio05.ui_mortalkombat.presentation.characters.Mk11CharactersListRepo
import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable

class GraphProvider(
    private val mk11CharactersRepo: Mk11CharactersListRepo,
    private val appReadyStateHolder: AppReadyStateHolder,
) {
    fun provide(
        builder: NavGraphBuilder,
        navController: NavController,
    ) = with(builder) {
        val mk11 = "mk11"
        val dota2 = "dota2"
        composable(GraphNode.Home.route) {
            HomeScreen(
                router = HomeRouter(
                    toMk11 = navController { isSingle ->
                        navigate(GraphNode.Characters.createRoute(mk11)) {
                            if (isSingle) popBackStack()
                        }
                    },
                    toDota2 = navController { isSingle ->
                        navigate(GraphNode.Characters.createRoute(dota2)) {
                            if (isSingle) popBackStack()
                        }
                    },
                ),
                onDataReady = appReadyStateHolder::invoke
            )
        }
        composable(route = GraphNode.Characters.route) { entry ->
            when (entry.arguments?.getString(GraphNode.Characters.argKey)) {
                mk11 -> Mk11Theme {
                    CharactersScreen(
                        columns = 3,
                        getList = suspend { mk11CharactersRepo(::Character) },
                        navigateTo = navController<String> {
                            navigate(route = GraphNode.Mk11Character.createRoute(it))
                        },
                        onDataReady = appReadyStateHolder::invoke
                    )
                }
            }
        }

        composable(route = GraphNode.Mk11Character.route) { entry ->
            entry.arguments?.getString(GraphNode.Mk11Character.argKey)?.let { id ->
                Mk11CharacterScreen(id = id)
            }
        }
    }
}
