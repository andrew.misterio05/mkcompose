package andrew.misterio.mkcompose

import andrew.misterio.mkcompose.di.diMain
import andrew.misterio.ui_characters.diCharacters
import andrew.misterio.ui_home.diHome
import andrew.misterio05.ui_mortalkombat.diMortalKombat11
import android.app.Application
import coil.ImageLoader
import coil.ImageLoaderFactory
import com.google.gson.Gson
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.module
import timber.log.Timber

class MkApp : Application(), ImageLoaderFactory {
    override fun newImageLoader(): ImageLoader = ImageLoader.Builder(this)
        .crossfade(true)
        .build()

    override fun onCreate() {
        super.onCreate()
        initTimber()
        startKoin {
            androidContext(this@MkApp)
            modules(
                module {
                    singleOf(::Gson)
                },
                diMain,
                diCharacters,
                diMortalKombat11,
                diHome,
            )
        }
    }

    private fun initTimber() {
        if (BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())
    }
}
