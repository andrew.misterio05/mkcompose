package andrew.misterio.mkcompose.ui.theme

import andrew.misterio.mkcompose.R
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.colorResource

@Composable
fun darkColorPalette() = darkColors(
    primary = colorResource(R.color.purple_200),
    primaryVariant = colorResource(R.color.purple_700),
    secondary = colorResource(R.color.teal_200),
)

@Composable
fun lightColorPalette() = lightColors(
    primary = colorResource(R.color.purple_500),
    primaryVariant = colorResource(R.color.purple_700),
    secondary = colorResource(R.color.teal_200),
)
