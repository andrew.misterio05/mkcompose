package andrew.misterio.mkcompose.ui.navigation

class AppReadyStateHolder {
    var state: Boolean = false
        private set

    operator fun invoke() {
        state = true
    }
}
