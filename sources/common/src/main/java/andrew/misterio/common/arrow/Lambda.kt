package andrew.misterio.common.arrow

typealias Lambda = () -> Unit
typealias Lambda1<T> = (T) -> Unit
