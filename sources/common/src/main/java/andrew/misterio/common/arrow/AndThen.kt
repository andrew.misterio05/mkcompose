package andrew.misterio.common.arrow

suspend fun <T, R> (suspend () -> T).andThen(
    other: (suspend (T) -> R)
): suspend () -> R = suspend { other(this()) }

suspend fun <T1, T2, R> (suspend (T1) -> T2).andThen(
    other: (suspend (T2) -> R)
): suspend (T1) -> R = { t1 -> other(this(t1)) }

suspend fun <T, R> (suspend () -> T).sAndThen(
    other: ((T) -> R)
): suspend () -> R = suspend { other(this()) }

suspend fun <T1, T2, R> (suspend (T1) -> T2).sAndThen(
    other: ((T2) -> R)
): suspend (T1) -> R = { t1 -> other(this(t1)) }

suspend fun <T, R> (() -> T).andThenS(
    other: (suspend (T) -> R)
): suspend () -> R = suspend { other(this()) }

suspend fun <T1, T2, R> ((T1) -> T2).andThenS(
    other: (suspend (T2) -> R)
): suspend (T1) -> R = { t1 -> other(this(t1)) }
